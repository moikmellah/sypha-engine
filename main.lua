--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

--love.window.setMode(0,0,{fullscreen=true})

require('classes.Environment')
--require('resource.extensions.Include')

env = Environment.new()

loadCoro = coroutine.create(function () env:loadConfig('./modules/durgankal/conf.xml') return true end)

function gameUpdate(dt)
	gconf.frameCt = (gconf.frameCt or 0) + 1
	env:update(dt)
end

function gameDraw()
	env:draw()

	love.graphics.push()

	love.graphics.setCanvas()

	local sx = love.graphics.getWidth()
	local sy = love.graphics.getHeight()

	local sfct = 1

	local mlt = 1

	local wide = (sx/sy >= 16/9)

	local ox = 0
	local oy = 0

	if(wide) then
		mlt = sy/gconf.vpHeight
		ox = (sx - (gconf.vpWidth * mlt)) / 2
	else
		mlt = sx/gconf.vpWidth
		oy = (sy - (gconf.vpHeight * mlt)) / 2
	end

	local sfc = env.vars.color

	if(sfc ~= nil) then
		love.graphics.setColor((sfc.r or 1)*255,(sfc.g or 1)*255,(sfc.b or 1)*255,(sfc.a or 1)*255)
		env.txtLib:setShader(sfc.shader)
	end

	love.graphics.draw(env.cnv, env.cnvQuad, ox,oy, 0, mlt, mlt)

	env.txtLib:setShader()
	love.graphics.setColor(255,255,255,255)
	
	love.graphics.pop()

	if((gconf.debugVerbosity ~= nil) and (gconf.debugVerbosity > 0)) then
		love.graphics.print('FPS: ' .. love.timer.getFPS() 
			--.. '; ' .. math.ceil(collectgarbage("count"))
			.. '; ' .. QuadCell.checks
			.. '; ' .. sx
			, 10, 10)
	end

end


function loadingUpdate(dt)

	local loadComplete = false

	local stat = 0

	env.vars.loadingR = (env.vars.loadingR or 0) + (dt * 2)

	stat,loadComplete = coroutine.resume(loadCoro)
	
	if(stat == false) then
		print(loadComplete)
		love.event.quit()
	end

	if(loadComplete == true) then
		love.draw = gameDraw
		love.update = gameUpdate
		gameUpdate(0.017)
	end

end

love.update = loadingUpdate

local sx = love.graphics.getWidth()
local sy = love.graphics.getHeight()

love.draw = function() env:loadingScreen("LOADING..", sx, sy) end


