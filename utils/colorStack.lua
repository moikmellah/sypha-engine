--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--


colorStack = {}

function setColor(r,g,b,a)

	local c = {}

	c.r, c.g, c.b, c.a = love.graphics.getColor()

	table.insert(colorStack, c)

	love.graphics.setColor(r,g,b,a)

end

function popColor()

	if(#colorStack > 0) then
		local c = table.remove(colorStack)
		love.graphics.setColor(c.r, c.g, c.b, c.a)
	end

end
