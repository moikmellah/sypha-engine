--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

--[[

	xml.lua - Provides xml parsing functionality.

	Adapted from code by Roberto Ierusalimschy; see http://lua-users.org/wiki/LuaXml
		for original source.

	This code is freely distributable under the zLib license.

--]]

require('utils.Inheritance')

function xmlParseArgs(s)
  local arg = {}
  string.gsub(s, "(%w+)=([\"'])(.-)%2", function (w, _, a)
    arg[w] = tonumber(a) or a
  end)
  return arg
end
    
function xmlCollect(s)
  local stack = {}
  local top = {}
  table.insert(stack, top)
  local ni,c,label,xarg, empty
  local i, j = 1, 1
  while true do
    ni,j,c,label,xarg, empty = string.find(s, "<(%/?)([%w:]+)(.-)(%/?)>", i)
    if not ni then break end
    local text = string.sub(s, i, ni-1)
    if not string.find(text, "^%s*$") then
      table.insert(top, text)
    end
    if empty == "/" then  -- empty element tag
      table.insert(top, {label=label, xarg=xmlParseArgs(xarg), empty=1})
    elseif c == "" then   -- start tag
      top = {label=label, xarg=xmlParseArgs(xarg)}
      table.insert(stack, top)   -- new level
    else  -- end tag
      local toclose = table.remove(stack)  -- remove top
      top = stack[#stack]
      if #stack < 1 then
        error("nothing to close with "..label)
      end
      if toclose.label ~= label then
        error("trying to close "..toclose.label.." with "..label)
      end
      table.insert(top, toclose)
    end
    i = j+1
  end
  local text = string.sub(s, i)
  if not string.find(text, "^%s*$") then
    table.insert(stack[#stack], text)
  end
  if #stack > 1 then
    error("unclosed "..stack[#stack].label)
  end
  return stack[1]
end

function xmlLoadFile(xmlPath)

	-- Just return if no path given.
	if xmlPath == nil then return nil end
--[[
	-- Attempt to open; return nil on failure.
	local fileHandle = io.open(xmlPath, 'r')
	if fileHandle == nil then return nil end

	-- Attempt to read; return nil on failure.
	local xmlStr = fileHandle:read('*a')
]]--

	xmlStr = love.filesystem.read(xmlPath)

	if xmlStr == nil then return nil end

	local xmlObj = xmlCollect(xmlStr)

	return xmlObj

end

function hasValidIndices(t_xml)

	for k,v in pairs(t_xml) do
	--	local s,e = string.find(k, "[^%a]")
		local s,e = string.find(k, "^[^%a]")
		if s then return false end
		s,e = string.find(k, "[^%w%-%.%_]")
		if s then return false end
	end

	return true

end

function serialize(tag, k, v, depth, ign)

	local ignore = ign

	if(ignore) then
		for str in string.gmatch(ignore, "[^|]+") do
			if k == str then return "" end
		end
	end

	local returnStr = ""
	local openTag = tag
	local tagName = ""
	local tagIndex = ""

	local tagContent = ""

	local indent = ""

	local valid = true

	for i = 1,depth do
		indent = indent .. "\t"
	end

	if(type(v) == 'string' or type(v) == 'number') then
		tagContent = tagContent .. v
	elseif(type(v) == 'boolean') then
		local bVal = 'false'
		if v then bVal = 'true' end
		tagContent = tagContent .. bVal 
	elseif(type(v) == "table") then
		valid = hasValidIndices(v)

		tagIndex = v.id or k

		--if not(valid) then openTag = openTag .. "s" end

		tagContent = tagContent .. "\n"

		local lign = v.ignore or ignore

		for ik,iv in pairs(v) do

			if valid then
				tagContent = tagContent .. serialize(ik, ik, iv, depth + 1, lign)
			else
				tagContent = tagContent .. serialize(tag, ik, iv, depth + 1, lign)
			end

		end

		tagContent = tagContent .. indent
	else
		return ""
	end

	tagName = openTag
	-- Assemble it
	if(tag ~= k) then openTag = openTag .. " id='" .. tagIndex .. "'" end

	returnStr = returnStr .. indent .. "<"..openTag..">"
	returnStr = returnStr .. tagContent
	returnStr = returnStr .. "</" .. tagName .. ">\n"

	return returnStr

end

Xml = {}

function Xml.deserializeFile(s_file, indexRules)

	if s_file == nil then return nil end

	local s_xml = love.filesystem.read(s_file)

	if(s_xml == nil) then return nil end

	return Xml.deserialize(s_xml, indexRules)

end

function Xml.deserialize(strXml, indexRules)

	local sArg = {}

	-- scrub out comments/meta
	local pass1 = string.gsub(strXml, '<!%-%-(.-)%-%->', '')

	-- Not the appropriate place for entity replacement - that should be up to the consumer of the loaded XML.
	-- OTOH, f**k it.  Works for now.
	pass1 = string.gsub(pass1, '&lt;', '<')
	pass1 = string.gsub(pass1, '&lte;', '<=')
	pass1 = string.gsub(pass1, '&gt;', '>')
	pass1 = string.gsub(pass1, '&gte;', '>=')

	sArg.s_xml = string.gsub(pass1, '<%?(.-)%?>', '')

	local retTab = Xml.deserialize_element(sArg)

	Xml.cleanup(retTab)

	if(type(retTab) == 'table') then
		local fk = next(retTab)
		return retTab[fk]
	end

	return nil

end

function Xml.deserialize_element(sArg, tag, start, forceArray)

	local thing = nil

	local isTable = false

	local s,e = start or 1, start or 1

	local ne = start or 1

	local content,close,t,xarg,empty

	while true do

		local primIndex = nil

		local child = nil

		s,e,content,close,t,xarg,empty = string.find(sArg.s_xml, "(.-)<(%/?)([%w%-%.%_]+)(.-)(%/?)>", e)

		--print('('..content..'-'..close..'-'..t..'-'..xarg..'-'..empty..')')

		if s == nil then
			return thing,e
			--error("Reached end without closing ..?")
		end

		if close == "/" then
			if t ~= tag then
				error("Didn't close '"..tag.."' properly.")
			elseif string.find(content, '[^%s]') then
				local val = tonumber(content) or string.gsub(content, '\\n', '\n')
				if type(thing) ~= 'table' then
					thing = val
				else
					table.insert(thing, val)
				end
			else
				break
			end
			-- Convert boolean values
			if(type(thing) == 'string') then
				local boolStr = string.match(thing, '^[%s]*(true)[%s]*$') or string.match(thing, '^[%s]*(false)[%s]*$')
				if boolStr == 'true' then
					thing = true
				elseif boolStr == 'false' then
					thing = false
				end
			end

			return thing,e+1
		else
			if type(thing) == 'nil' then thing = {} end
			
			if string.find(xarg, '[^%s]') then
				child = xmlParseArgs(xarg)
				primIndex = child.id or child.name or t
			end

			if empty ~= "/" then
				local val = nil
				val,e = Xml.deserialize_element(sArg, t, e+1)
				if(type(val) == 'table' and type(child) == 'table') then 
					child = mergeTables(child, val) 
--				elseif(type(child) == 'table') then
--					child.content = val
				else
					child = val
				end
			else
				e = e + 1
			end
				-- order of pref: id, name, tag
			local myIndex = t
			local myTag = t
			if type(child) == 'table' then
				myIndex = child.id or child.name or t
			else
				myIndex = primIndex or t
			end

			-- If it's named same as parent, just use index or insert it.
			if(false and myTag == tag) then
					
				if(myTag == myIndex) then
					table.insert(thing[myIndex], child)
				else
					thing[myIndex] = child
				end

			else
			-- check for dupe keys
				if(myIndex ~= myTag and not(thing[myTag])) then
					thing[myTag] = {useInserts = true}
				end

				if(type(thing[myTag]) == 'table' and not thing[myTag].useInserts) then
					local temp = thing[myTag]
					local tIndex = temp.id or temp.name or myTag
					thing[myTag] = {useInserts = true}
					--table.insert(thing[myTag], temp)
					if(myTag == tIndex) then
						table.insert(thing[myTag], temp)
					else
						thing[myTag][tIndex] = temp
					end
				elseif(thing[myTag] ~= nil and type(thing[myTag]) ~= 'table') then
					local temp = thing[myTag]
					thing[myTag] = {useInserts = true}
					table.insert(thing[myTag], temp)
				end
	
				-- Insert it
				if(type(thing[myTag]) == 'table' and thing[myTag].useInserts) then
					if(myTag == myIndex) then
						table.insert(thing[myTag], child)
					else
						thing[myTag][myIndex] = child
					end
				else
					thing[myTag] = child
				end
			end
			
		end

	end

	return thing,e+1

end


function Xml.serialize(t_tab, s_tag, s_id, i_depth)

	local tag = t_tab.class or s_tag or 'node'
	local id = t_tab.id or s_id or 'node'
	local rules = t_tab.ignore
	local depth = tonumber(i_depth) or 0

	return Xml:serialize_element(t_tab, tag, id, depth, rules)

end

function Xml:serialize_element(v, tag, k, depth, ign)

	local ignore = (ign or '') .. '|id|useInserts'

	if(ignore) then
		for str in string.gmatch(ignore, "[^|]+") do
			if k == str then return "" end
		end
	end

	local returnStr = ""
	local openTag = tag
	local tagName = ""
	local tagIndex = ""

	local tagContent = ""

	local indent = ""

	local valid = true

	for i = 1,depth do
		indent = indent .. "\t"
	end

	if(type(v) == 'string' or type(v) == 'number') then
		tagContent = tagContent .. v
		tagIndex = k
	elseif(type(v) == 'boolean') then
		local bVal = 'false'
		if v then bVal = 'true' end
		tagContent = tagContent .. bVal 
		tagIndex = k
	elseif(type(v) == "table") then
		valid = hasValidIndices(v)

		tagIndex = v.id or k

		if (valid) then tagContent = tagContent .. "\n" end

		local lign = v.ignore or ignore

		local ktab = {}

		for tk in pairs(v) do table.insert(ktab, tk) end

		local sortFct = function(a,b)
			local ta = type(a)
			local tb = type(b)
			if ta == 'number' and tb == 'string' then return true
			elseif ta == 'string' and tb == 'number' then return false
			elseif ta == tb then return (a < b)
			else return true
			end
		end

		table.sort(ktab, sortFct)

		for i = 1,#ktab do

			local ik = ktab[i]
			local iv = v[ik]

			if valid then
				tagContent = tagContent .. self:serialize_element(iv, ik, ik, depth + 1, lign)
			else
				tagContent = tagContent .. self:serialize_element(iv, tag, ik, depth, lign)
			end

		end

		if(valid) then tagContent = tagContent .. indent end
	else
		return ""
	end

	tagName = openTag
	-- Assemble it
	if(tag ~= k) then openTag = openTag .. " id='" .. tagIndex .. "'" end

	if(valid) then returnStr = returnStr .. indent .. "<"..openTag..">" end
	returnStr = returnStr .. tagContent
	if(valid) then returnStr = returnStr .. "</" .. tagName .. ">\n" end

	return returnStr

end

function Xml.cleanup(t_xml)
	if(type(t_xml) ~= 'table') then return false end
	t_xml.useInserts = nil
	for k,v in pairs(t_xml) do
		if(type(v) == 'table') then
			Xml.cleanup(v)
		end
	end

	return true
end
