--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

gconf = gconf or {}

-- tableCopy(a) - recursively returns a full copy of table 'a' (minus threads)
function tableCopy(a)

	local b = nil

	if type(a) == "table" then

		b = {}

		for k,v in pairs(a) do

			if type(v) == "table" then
				b[k] = tableCopy(a[k])
			elseif type(v) ~= "thread" then
				b[k] = a[k]
			end

		end

	end

	return b

end



--[[

	objDepthComp(obj a, obj b) - Compares relative depth of two objects.  Used for table sort.

	Question: Are 'nil' values even compared?  A: They shouldn't be.  Remove obsolete objs with table.remove().

]]--

function objRenderSort(a,b)

	-- Next: capture depth values and compare.  Lower depth (farther back) renders first
	local aDep = a.z or 0
	local bDep = b.z or 0
	if aDep < bDep then return true end
	if bDep < aDep then return false end

	-- Still equal?  Try y values, then x values
	local ay = a.y or 0
	local by = b.y or 0
	if ay < by then return true end
	if by < ay then return false end
--[[	local ax = a.x or 0
	local bx = b.x or 0
	if ax > bx then return true end
	if bx > ax then return false end]]--

	-- STILL equal?  Use 'tostring' as a tiebreaker (should return address of table in memory ..?)
	local ai = tostring(a)
	local bi = tostring(b)
	if ai > bi then return true else return false end

end

function objPrioSort(a,b)

	local ap = a.prio or 0
	local bp = b.prio or 0

	if(ap < bp) then return true end
	if(bp < ap) then return false end

	-- Tiebreaker
	local ai = tostring(a)
	local bi = tostring(b)
	if ai < bi then return true else return false end

end

function nothing()

	while 1 == 1 do
		coroutine.yield()
	end

end

function fileToString(file)

	local str = ""

	local fh = io.open(file)
	str = str..fh:read("*a")
	fh:close()

	return str

end

function tryParse(val)
	return tonumber(val) or val
end

function tryParseInt(val)
	return tonumber(val) or val
end

function defParse(val, default)
	return tonumber(val) or default or 0
end

-- Intentionally shallow.  Deal.
function tableMerge(a,b,overwrite)

	local ao = (type(a) == 'table')
	local bo = (type(b) == 'table')
	
	if(ao ~= true) then return a end
	
	if(bo ~= true) then return b end
	
	--for(var k in b){
	for k,v in pairs(b) do
		if ((a[k] == nil) or (overwrite == true)) then
			a[k] = v
		end
	end

	return a
	
end


function round(n)
	return math.floor(n+.5)
end

function units(value)

	return value / gconf.ppu
end

function pixels(value)

	return round(value * gconf.ppu)
end

function debugPrintTable(xmlNode,depth,priority)

	local vrb = gconf.debugVerbosity or 0
	local prio = priority or 3

	if((vrb == 0) or (vrb < prio)) then return end

	local first = ''

	local k,v

	local d = depth or 0

	if(d > 3) then return end

	for c = 1,d do first = first..'\t' end

	for k,v in pairs(xmlNode) do
		if(type(v) == 'table') then 
			print(first..k..':')
			debugPrintTable(v, d+1, prio) 
		elseif(type(v) == 'function') then
			print(first..k..'(f)')
		elseif(type(v) == 'string' or type(v) == 'number') then
			print(first..k..'('..string.sub(type(v),1,1)..'): '..v)
		end
	end

end

function debugPrint(dString, priority)

	gconf.frameCt = gconf.frameCt or 0

	local vrb = gconf.debugVerbosity or 0
	local prio = priority or 3

	if((vrb == 0) or (vrb < prio)) then return end

	if(type(dString) == 'string' or type(dString) == 'number') then
		print(gconf.frameCt .. ': ' .. dString)
	elseif(type(dString) == 'table') then
		debugPrintTable(dString, 0, priority)
	end

end

function debugConsole()

	print("[Lua debug console]\n\tEnter valid lua command(s) and then 'run' to execute, or type 'quit' to return to game:")

	local inputLine = ""
	local str = ""
	
	while(inputLine ~= "quit") do
		if(inputLine == "run") then

			local st, err = pcall(assert(loadstring(str)))
			if not st then print(err) end
			str = ""
			inputLine = ""

		else
			str = str .. "\n" .. inputLine
			inputLine = io.input():read()
		end
	end

end

function clamp(min,x,max)

	if(type(min) ~= 'number') then return x end
	if(type(max) ~= 'number') then return x end
	if(type(x) ~= 'number') then return x end
	
	return math.max(min,math.min(x,max))

end

function dist(x1,y1,x2,y2)
	return math.sqrt(((x2-x1)^2)+((y2-y1)^2))
end

function contains(subject,pattern)

	if(type(subject) == 'string') then
		return (string.find(subject, pattern) ~= nil)
	elseif(type(subject) == 'table') then
		for k,v in pairs(subject) do
			if(v == pattern) then return true end
		end
	end

end

string.camel = function(a)
	local b = string.gsub(a,'^%a',f)
	b = string.gsub(b,'[^%a]%a',f)
	return b
end
