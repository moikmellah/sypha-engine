--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

--[[

	andor.lua - A few simple bitwise operations.  Based on code snippet by Arno Wagner, found at:
		http://lua-users.org/wiki/BitUtils

]]--

function b_xor(x, y)
	local z = 0
	for i = 0, 31 do
		if (x % 2 == 0) then							 -- x had a '0' in bit i
			if ( y % 2 == 1) then						-- y had a '1' in bit i
				y = y - 1 
				z = z + 2 ^ i							  -- set bit i of z to '1' 
			end
		else												  -- x had a '1' in bit i
			x = x - 1
			if (y % 2 == 0) then						-- y had a '0' in bit i
				z = z + 2 ^ i							  -- set bit i of z to '1' 
			else
				y = y - 1 
			end
		end
		y = y / 2
		x = x / 2
	end
	return z
end


function b_or(x, y)
	local z = 0
	local doit
	for i = 0, 31 do
		doit = false
		if (x % 2 == 1) then doit = true x = x - 1 end							 -- x had a '0' in bit i
		if ( y % 2 == 1) then doit = true y = y - 1 end						-- y had a '1' in bit i
		if (doit) then z = z + 2 ^ i end
		y = y / 2
		x = x / 2
	end
	return z
end

function b_and(x, y)
	local z = 0
	local doit
	for i = 0, 31 do
		doit = true
		if (x % 2 == 1) then x = x - 1 else doit = false end							 -- x had a '0' in bit i
		if ( y % 2 == 1) then y = y - 1 else doit = false end						-- y had a '1' in bit i
		if (doit) then z = z + 2 ^ i end
		y = y / 2
		x = x / 2
	end
	return z
end
