--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

ppu = 16

vpKeepAR = false
vpWidth = ppu*32
vpHeight = ppu*18
bq = nil

debugVerbosity = 9

frameCt = 0

DEFAULTCOLOR = {r=1,g=1,b=1,a=1}

SCENELBX = 16
SCENELBY = 2

