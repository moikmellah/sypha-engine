--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

if((love._version_major == 0 and love._version_minor >= 10) or love._version_major >= 11) then
	LOVESPACE = 'space'
else
	LOVESPACE = ' '
end

if(love._version_major >= 11) then
	love.audio.rewind = function (source) source:seek(0) end
	love.filesystem.exists = function (path) return (love.filesystem.getInfo(path) ~= nil) end

	local lgsc = love.graphics.setColor
	love.graphics.setColor = function(r,g,b,a)
		lgsc((r or 0)/255,(g or 0)/255,(b or 0)/255,(a or 0)/255)
	end
	
	local lgc = love.graphics.clear
	love.graphics.clear = function(r,g,b,a)
		lgc((r or 0)/255,(g or 0)/255,(b or 0)/255,(a or 0)/255)
	end
end

if((love._version_major == 0 and love._version_minor >= 9) or love._version_major >= 11) then
	love.graphics.drawq = love.graphics.draw
	love.graphics.quad = love.graphics.polygon
	love.graphics.setDefaultImageFilter = love.graphics.setDefaultFilter
	love.graphics.toggleFullscreen = function () 
		local isFS = love.window.getFullscreen() 
		love.window.setFullscreen(not(isFS)) 
		local newWidth,newHeight = love.window.getDimensions()
		if (not(isFS) and ((newHeight / newWidth) < .75)) then
			winOfs = (newWidth - (newHeight * 4 / 3)) / (love.graphics.getHeight() / 240 * 2)
			--print(winOfs..'('..newWidth..';'..newHeight..')')
		else
			winOfs = 0
		end
	end
elseif(love._version_major == 0 and love._version_minor == 8) then
	love.window = love.graphics
end 
