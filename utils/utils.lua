--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

-- full utils library

bit = require('bit')
require('utils.compat')
--require('utils.globals')
require('utils.misc')
require('utils.xml')
require('utils.stringSplit')

