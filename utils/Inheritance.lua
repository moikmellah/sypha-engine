--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

-- Inherit (actually just a simple table merge)
function Inherit(toClass, fromClass)

	local mergedClass = nil

	if(type(toClass) ~= 'table') then 
		mergedClass = {}
	else
		mergedClass = toClass
	end

	if(type(fromClass) ~= 'table') then
		return mergedClass
	end

	for k,v in pairs(fromClass) do

		mergedClass[k] = v

	end

	return mergedClass

end

mergeTables = Inherit
