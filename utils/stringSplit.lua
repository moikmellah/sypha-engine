--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

--[[

	strToTable(): Basically the equivalent of an explode() or split() function.  Takes a
		string and a separator, and uses that to split a single string into an indexed
		table.

		As a note, it will automatically convert to numeric formats when available.

]]--
function strToTable(aStr,sep,rev)

	if(type(aStr) ~= 'string') then return nil end
	local mySep = sep or '%s'
	if(string.len(mySep) == 0) then mySep = ',' end	
	local myPat = '([^'..mySep..']+)'..mySep..'?'
	local nxStart = 0
	local nxEnd = -1
	local nxElement = true
	local retArray = {}

	if not rev then
		while nxStart ~= nil do
			nxStart, nxEnd, nxElement = string.find(aStr, myPat, nxEnd + 1)
			retArray[#retArray + 1] = tonumber(nxElement) or nxElement
		end
	else
		local indPat = '([^'..mySep..']+)'
		local revPat = indPat .. '$'
		while nxStart ~= nil do
			nxStart, nxEnd, nxElement = string.find(aStr, revPat)
			retArray[#retArray + 1] = tonumber(nxElement) or nxElement
			revPat = indPat .. mySep .. revPat
		end
	end

	return retArray

end

--[[

	strSplit(): This should do the same as the above, but preferably without tables.
		(e.g. Uses recursion, and returns a list of values instead of a table.)

		Pro: Less overhead (no table creation, etc.)
		Con: Lists are less flexible.

]]--

function strSplit(aStr, sep, aStart)

	if(type(aStr) ~= 'string') then return nil end
	local mySep = sep or '%s'
	if(string.len(mySep) == 0) then mySep = '%s' end	
	local myPat = '([^'..mySep..']+)'..mySep..'?'
	local myStart = aStart or 0
	local nxStart, nxEnd, myElement = string.find(aStr, myPat, aStart)

	local myRet = tonumber(myElement) or myElement

	if(myRet) then
		return myRet, strSplit(aStr, sep, nxEnd + 1)
	else
		return myRet
	end

end

function strSplitRev(aStr, sep, pat)

	if(type(aStr) ~= 'string') then return nil end
	local mySep = sep or '%s'
	if(string.len(mySep) == 0) then mySep = '%s' end	
	local indPat = '([^'..mySep..']+)'
	local myPat = pat or '$'
	local revPat = indPat .. myPat
	local myStart = aStart or 0
	local nxStart, nxEnd, myElement = string.find(aStr, revPat, aStart)

	local myRet = tonumber(myElement) or myElement

	if(myRet) then
		return myRet, strSplitRev(aStr, sep, mySep..revPat)
	else
		return myRet
	end

end

function pathToFile(s_file)

	local filePath = s_file or './'

	local pathArray = strToTable(filePath, '/')

	local file = table.remove(pathArray)

	if(pathArray[1] == '..') then
		table.remove(pathArray, 1)
	end

	-- Kill any 'blah/..' pairs
	local k = #pathArray

	local cleanPath = {}

	for i,w in ipairs(pathArray) do

		if(w == '..') then
			table.remove(cleanPath)
		elseif(w ~= '.' and w ~= '' and (type(w) == 'string' or type(w) == 'number')) then
			table.insert(cleanPath, w)
		end

	end

--[[	while k > 1 do

		local v = pathArray[k]

		if(v == '.') then
			table.remove(pathArray, k)
		elseif(v == '..') then
			table.remove(pathArray, k)
			k = k - 1
			table.remove(pathArray, k)
		end

		k = k - 1

	end
]]--
	local path = nil

	for k,v in ipairs(cleanPath) do
		if path == nil then
			path = v
		else
			path = path .. '/' .. v
		end
	end

	path = path or '.'

	return path..'/'..file,path,file

end
