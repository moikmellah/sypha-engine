function love.conf(t)
	local ts = nil

	if((love._version_major == 0 and love._version_minor >= 9) or love._version_major >= 11) then
		ts = t.window
	else
		ts = t.screen
	end

	t.title = "Project SYPHA"
	ts.width = 16*32*2
	ts.height = 16*18*2
	ts.resizable = true
	ts.minwidth = 16*32
	ts.minheight = 16*18
	--t.screen.width = 320
	--t.screen.height = 240
	ts.fullscreen = false
	t.console = true
end
