--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
ItemLibrary = {}

-- Metatable for class (to make ItemLibrary table the default lookup for class methods)
ItemLibrary_mt = {}
ItemLibrary_mt.__index = ItemLibrary

-- Constructor
function ItemLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nActLib = {}

	-- Set metatable to get our class methods
	setmetatable(nActLib, ItemLibrary_mt)

	nActLib.items = {}
	nActLib.count = 0

	nActLib.list = {}

	return nActLib

end

function ItemLibrary:init()

	if self.items == nil then self.items = {} self.count = 0 end
	
	self.list = self.list or {}

end

function ItemLibrary:loadSet(s_fileName)

	--if(s_handle == nil) then return false end

	self:init()

	if(s_fileName == nil) then return false end

	if self.items == nil then self.items = {} end

	--if self.ItemSet[handle] ~= nil then return true end

	local aS = Xml.deserializeFile(s_fileName)

	for k,v in pairs(aS.item) do
		self.items[k] = v
	end

end

function ItemLibrary:buildLists()

	self.list = {}

	for k,v in pairs(self.mods) do
	
		local tier = v.tier or 1
		local iClass = v.class or 'melee'
		local aTab = strToTable(v.attrs, ',')
		
		--Calculate item cat
		
		-- Add to each item class list
		for ik,iv in pairs(iTab) do
			self.list[iv] = self.list[iv] or {}
			self.list[iv][iType] = self.list[iv][iType] or {}
			self.list[iv][iType][tier] = self.list[iv][iType][tier] or {}
			table.insert(self.list[iv][iType][tier], v.id)
		end
	
	end

	return nil	

end

