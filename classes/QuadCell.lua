--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
QuadCell = {}

-- Metatable for class (to make QuadTree table the default lookup for class methods)
QuadCell_mt = {}
QuadCell_mt.__index = QuadCell

QuadCell.count = 0
QuadCell.maxCount = 99999

QuadCell.checks = 0
QuadCell.colls = 0

QuadCell.maintDelay = 3

QuadCell.minSize = 4

QuadCell.itemThreshold = 6

-- Constructor
function QuadCell.new(parent, x, y, width, height, innerQuad)

	QuadCell.count = QuadCell.count or 0
	QuadCell.maxCount = QuadCell.maxCount or 10000

	if (QuadCell.count > QuadCell.maxCount) then return nil end

	local nQuad = {}	

	setmetatable(nQuad, QuadCell_mt)

	--// Set dimensions
	nQuad.x = x
	nQuad.y = y
	nQuad.width = width
	nQuad.height = height
	nQuad.bnd = {}
	nQuad.bnd[0] = x
	nQuad.bnd[1] = y
	nQuad.bnd[2] = x + width
	nQuad.bnd[3] = y + height
	
	--// Set parent node
	nQuad.parent = parent

	--// Create table for contained objects
	nQuad.objects = {}
	nQuad.tmpobj = {}

	nQuad.quads = nil

	nQuad.itemThreshold = QuadCell.itemThreshold
	nQuad.maintDelay = QuadCell.maintDelay		--// Time to remain empty before collapsing, in seconds

	--// Typical minimum item size is 16x16, so that's our min, I guess.  Whatever.  Can be adjusted.
	nQuad.minSize = QuadCell.minSize	--// Duh, use world unit size.  pixels only pertain to rendering

	nQuad.readyTime = 0

	QuadCell.count = QuadCell.count + 1

	if(innerQuad ~= nil) then nQuad:subdivide(innerQuad) end
	
	return nQuad

end

function QuadCell:subdivide(innerQuad)

	--// Note: using zero-index for our child table

	--[[// If right of center line, index += 1; if below center line, index += 2
	//	0	1
	//	2	3]]--

	local halfWidth = self.width / 2
	local halfHeight = self.height / 2
	local inner = -1

	if (innerQuad ~= nil) then
		inner = 0
		if(innerQuad.x > self.x) then inner = inner + 1 end
		if(innerQuad.y > self.y) then inner = inner + 2 end
	end

	if((halfWidth < self.minSize) or (halfHeight < self.minSize)) then return false end

	self.quads = {}

	if(inner >= 0) then  
		self.quads[inner] = innerQuad
		innerQuad.parent = self
	end

	self.quads[0] = self.quads[0] or QuadCell.new(self, self.x, self.y, halfWidth, halfHeight)
	self.quads[1] = self.quads[1] or QuadCell.new(self, self.x + halfWidth, self.y, halfWidth, halfHeight)
	self.quads[2] = self.quads[2] or QuadCell.new(self, self.x, self.y + halfHeight, halfWidth, halfHeight)
	self.quads[3] = self.quads[3] or QuadCell.new(self, self.x + halfWidth, self.y + halfHeight, halfWidth, halfHeight)

	--// Place objects again, in case 
	--// Count backward, to avoid reordering from the table.remove() call.
--	for c = #self.objects,1,-1 do
	--for(local c = self.objects.length; c >= 0; c--){

--		local tmpObj = self.objects[c]

		--self.objects.splice(c, 1)
--		table.remove(self.objects,c)

		--// Places the object on the } of the array, so should be safe.  Probably.
--		self:place(tmpObj)

--	end

	local tmp = self.tmpobj
	self.tmpobj = self.objects
	self.objects = tmp
	
	while(#self.tmpobj > 0) do

		--self.place(self.tmpobj.pop(), nil);
		self:place(table.remove(self.tmpobj))
		
	end


end

function QuadCell:itemCount()

	local retCount = 0

	retCount = retCount + #self.objects

	if (self.quads ~= nil) then
		--// Can't do standard indexed ops, since we use zero-indexing.
		for c = 0,3 do
		--for(local c = 0; c < 4; c++)
			retCount = retCount + self.quads[c]:itemCount()
		end
	end

	return retCount

end

function QuadCell:contains(obj)

	--// Tables only, please.
	if(obj == nil) then return false end

	--// **TODO: Implement getBounds on GameObject class.
	local o = obj:getBounds()

	local q = self:getBounds()

	--// Return a simple bool.
	return ((o[0] >= q[0]) 
		and (o[2] < q[2]) 
		and (o[1] >= q[1]) 
		and (o[3] < q[3]))

end

function QuadCell:getBounds()

	return self.bnd

end

function QuadCell:place(obj,from)

	--// Tables only, please.
	if(obj == nil) then return false end

	local doesFit = self:contains(obj)

	--// Not contained within self cell 
	if (doesFit ~= true) then 

		local sp = self.parent

		--// Do we have a Parent cell?
		if ((sp ~= nil) and (sp ~= from)) then

			--// Hand it off to parent.
			return self.parent:place(obj,self)

		elseif (sp == nil) then
		
			local dbWidth = self.width * 2
			local dbHeight = self.height * 2
			local px = self.x
			local py = self.y
			local ob = obj:getBounds(true)

			if(ob[0] < self.x) then px = self.x - self.width end
			if(ob[1] < self.y) then py = self.y - self.height end

			local newQuad = QuadCell.new(nil, px, py, dbWidth, dbHeight, self)

			return newQuad:place(obj,self)

		end

	else

		--// Making an assumption: if it can have child nodes, then it always has all four.
		if (self.quads ~= nil) then
	
			--// Note: A bit hamfisted.  Has a possibility of checking the entire tree,
			--//	including the branch we just came from.  Reconsider.
			for c = 0,3 do
			--for(local c = 0; c < 4; c++){
				local sqc = self.quads[c]
				--// Check to see if each could contain it, before sending the obj down that branch.
				--// If we've placed it successfully, { we're done.
				--// Note: the 'contains' check is redundant now, 
				if((sqc ~= from) and sqc:contains(obj) and sqc:place(obj)) then 
					return true 
				end
	
			end
	
		end

		--// If we've gotten here, we need to contain it ourselves.
		--self.objects:push(obj)
		table.insert(self.objects, obj)
		obj.refs.collCell = self

		if ((#self.objects > self.itemThreshold) and (self.quads == nil)) then self:subdivide() end

		return true

	end

	return false

end

function QuadCell:plinko(obj)

	--// Tables/objects only, please.
	if(obj == nil) then return false end

	local doesFit = self:contains(obj)

	--// Not contained within self cell 
	if(doesFit == true) then

		--// Making an assumption: if it can have child nodes, then it always has all four.
		if (self.quads ~= nil) then
	
			--// Note: A bit hamfisted.  Has a possibility of checking the entire tree,
			--//	including the branch we just came from.  Reconsider.
			for c = 0,3 do
			--for(local c = 0; c < 4; c++)
			
	
				--// If we've placed it successfully, then we're done.
				--// Note: the 'contains' check is redundant now, 
				if(self.quads[c]:plinko(obj)) then return true end
	
			end
	
		end

		--// Didn't fit any children, so keep it here.
		--self.objects.push(obj);
		table.insert(self.objects, obj)

		--// Subdivide here, assuming we're over the minimum size threshold.
		if ((self.quads == nil) and (#self.objects > self.itemThreshold)) then self:subdivide() end

		return true

	end

	return false

end

function QuadCell:getLength()

	local returnLen = 1

	local longest = 0

	if(self.quads ~= nil) then
		--for(local c = 0; c < 4; c++)
		for c = 0,3 do
			longest = math.max(longest, self.quads[c]:getLength())
		end
	end

	return returnLen + longest

end

function QuadCell:getRoot()

	if(self.parent) then return self.parent:getRoot() end
	
	return self

end

function QuadCell:doMaint(dt)

	local delta = dt or 1

	--// First, if no child cells, let the calling function know we're OK to be pruned.
	if(self.quads == nil) then
		self.readyTime = self.readyTime + delta
		return true
	else
		self.readyTime = 0
	end

	local pruneOk = true
	local minReadyTime = self.maintDelay		--// Start at threshold.

	--// Check whether all children are empty and ready for pruning. 
	--for(local c = 0; c < 4; c++)
	for c = 0,3 do
		pruneOk = (pruneOk and self.quads[c]:doMaint(delta))
		minReadyTime = math.min(minReadyTime, self.quads[c].readyTime)
	end

	--// All children are leaf nodes, so we're good there.
	if (pruneOk == true) then

		--// Don't prune if we still have enough items to force a subdivision again anyway.
		if((self:itemCount() <= self.itemThreshold) and (minReadyTime >= self.maintDelay)) then 

			--// Okay, let's prune.  Steal all objects, and set my children free.
			for c = 0,3 do
				for k,obj in pairs(self.quads[c].objects) do
					table.insert(self.objects, obj)
					obj.refs.collCell = self
				end
				self.quads[c] = nil

				QuadCell.count = QuadCell.count - 1
			end

			self.quads = nil

		end

	end

	return false

end

--// Total number of cells, recursively counted
function QuadCell:headCount()

	local retCount = 1

	local childCount = 0

	if (self.quads ~= nil) then
		--for(local c = 0; c < 4; c++)
		for c = 0,3 do
			childCount = childCount + self.quads[c]:headCount()
		end
	end

	return retCount + childCount

end

function QuadCell:shakeDown()

	QuadCell.checks = 0

	if(self.quads ~= nil) then
		--for(local k = 0; k < 4; k++)
		for k = 0,3 do
			self.quads[k]:shakeDown()
		end
	end

	--// Remove and re-place all objects in list
	--//for c = #self.objects,1,-1 do
	local tmp = self.tmpobj
	self.tmpobj = self.objects
	self.objects = tmp
	
	while(#self.tmpobj > 0) do

		--self.place(self.tmpobj.pop(), nil);
		self:place(table.remove(self.tmpobj))
		
	end

end

function QuadCell:doColls()

	--// Do child branches first.
	if(self.quads ~= nil) then

		--for(local q = 0; q < 4; q++)
		for q = 0,3 do
			self.quads[q]:doColls()
		end

	end

	for a = 1,#self.objects do
		if(self.objects[a]:canCollide()) then
			self:checkObj(self.objects[a], a+1)
		end
	end

end

function QuadCell:expireColls()

	--// Do child branches first.
	if(self.quads ~= nil) then

		--for(local q = 0; q < 4; q++)
		for q = 0,3 do
			self.quads[q]:expireColls()
		end

	end

	for a = 1,#self.objects do
	--for(local a = 0; a < self.objects.length; a++){
		self.objects[a]:expireColls()
	end

end

function QuadCell:removeColls()

	--// Do child branches first.
	if(self.quads ~= nil) then

		for q = 0,3 do
			self.quads[q]:removeColls()
		end

	end

	for a = 1,#self.objects do
		self.objects[a]:removeColls()
	end

end

function QuadCell:checkObj(obj, stIndex)

	if(obj == nil) then return false end
	local st = stIndex or 1

	for x = st,#self.objects do
		local myObj = self.objects[x]

		if(myObj:canCollide()) then

			local a = myObj:getBounds()
			local b = obj:getBounds()

			QuadCell.checks = QuadCell.checks + 1

			if ((a[0] <= b[2]) and (b[0] <= a[2]) and (a[1] <= b[3]) and (b[1] <= a[3])) then
		
				--// Silly counting - insert the real logic here.
				QuadCell.colls = QuadCell.colls + 1;
				obj:setCollision(myObj)
				myObj:setCollision(obj)
			end
		
		end
	end

	if (self.parent ~= nil) then return self.parent:checkObj(obj) end

end

function QuadCell:removeObj(obj)

	for k = #self.objects,1,-1 do
		if(self.objects[k] == obj) then
			table.remove(self.objects, k)
			break
		end
	end

	obj.refs.collCell = nil

end
