--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('classes.Actor')
require('utils.Inheritance')

Camera = Inherit(nil, Actor)

function Camera:draw(ctx, ctx2)

	local hw = units(gconf.vpWidth/2)
	local fhw = math.floor(hw)

	local hh = units(gconf.vpHeight/2)
	local fhh = math.floor(hh)

	local sm = (self.scene.env.vars.stereoMag or 0)
	local d = round((self.z or 0) * sm * 2) / 2
	
	if(self.scene.env.vars.reverseStereo) then d = -d end

	if(ctx2 == nil) then d = 0 end
	if(self.visible == 0) then return true end
	--//d *= 2;
	
	local ly = self.scene.tilemap[self.map].layer[self.layer]
	local x = self.x - hw - units(d)

	local y = self.y - hh
	
	local xDiff = math.abs(x - self.lzx)
	local yDiff = math.abs(y - self.lzy)
	
	if(xDiff > fhw or yDiff > fhh or self.dirty) then
		self.lzx = math.floor(x/fhw) * fhw
		self.lzy = math.floor(y/fhh) * fhh
		self.scene.tilemap[self.map]:renderLayer(self.layer, self.ctx, self.lzx, self.lzy)
	end
	
	--TODO: this
	--ctx.drawImage(self.canvas, pixels(x-(self.lzx-10)),pixels(y-(self.lzy-7)), 320,240, 0,0, 320,240);
	love.graphics.push()

	love.graphics.setCanvas(ctx)

	local bq = love.graphics.newQuad(pixels(x-(self.lzx-fhw)), pixels(y-(self.lzy-fhh)), gconf.vpWidth, gconf.vpHeight, gconf.vpWidth*2, gconf.vpHeight*2)

	love.graphics.draw(self.ctx, bq, 0, 0)

	love.graphics.pop()
	-- Only draw to right viewport if we're rendering stereo.
	if(ctx2 ~= nil) then
	
		--//Right screen	
		x = self.x - hw + units(d)
		y = self.y - hh
		
		xDiff = math.abs(x - self.lzx2)
		yDiff = math.abs(y - self.lzy2)
		
		if(xDiff > fhw or yDiff > fhh or self.dirty) then
			self.lzx2 = math.floor(x/fhw) * fhw
			self.lzy2 = math.floor(y/fhh) * fhh
			self.scene.tilemap[self.map]:renderLayer(self.layer, self.ctx2, self.lzx2, self.lzy2)
		end
		
		--TODO: this
		--ctx2.drawImage(self.canvas2, pixels(x-(self.lzx2-10)),pixels(y-(self.lzy2-7)), 320,240, 0,0, 320,240)
	end

	self.dirty = false

end
