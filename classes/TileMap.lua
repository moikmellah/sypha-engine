--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

TileMap = {}

TileMap_mt = {}
TileMap_mt.__index = TileMap

function TileMap.new(s_file, imgLib, tilesetLib)

	local nt = {}
	setmetatable(nt, TileMap_mt)

	local filePath,dirPath = pathToFile(s_file)

	local doc = Xml.deserializeFile(filePath)

	nt.tileSrc = {}
	local tileSrc = nt.tileSrc
	
	-- We should really pass in an imgLib, but here's some insurance.
	nt.imageSrc = imgLib or ImageLibrary.new()
	local imageSrc = nt.imageSrc
	
	nt.layer = {}
	local layer = nt.layer

	--local tileSets = self.doc.getElementsByTagName('tileset');

	local tileSets = doc.tileset

	nt.objectgroup = doc.objectgroup

	-- If it has an image tag, it's a proper tileset (and thus, the only one)
	if(tileSets.image ~= nil) then
		-- Make it a table, for proper looping
		tileSets = {}
		table.insert(doc.tileset)
	end
	
	--//load each tileset
	--for(tsX = 0; tsX < tileSets.length; tsX++){
	for tsX,ts in pairs(tileSets) do
		
		--local ts = tileSets[tsX];
		if(type(ts) == 'table' and type(ts.source) == 'string') then

			local s_src = pathToFile(dirPath .. '/' .. ts.source)

			-- Tiled map: tilesets and images are relative to the file itself.
			local t_ts = tilesetLib:loadSet(s_src)

			local firstGid = ts.firstgid

			--for k,v in ipairs(t_ts) do
			for k = 0,#t_ts.tiles do
				local v = t_ts.tiles[k]
				local id = k + firstGid
				tileSrc[id] = v
			end

		elseif(type(ts) == 'table') then
			local tmpts = {}
		
			local tsName = ts.name --ts.getAttribute('name');
		
			local imWidth
			local imHeight
		
			local tsWidth = ts.tilewidth --parseInt(ts.getAttribute('tilewidth'));
			local tsHeight = ts.tileheight --parseInt(ts.getAttribute('tileheight'));
	
			-- Should only be one for a given TileSet
			--local imageList = ts.image --ts.getElementsByTagName('image');
		
			--//load any referenced images
			local im = ts.image
		
			local path = pathToFile(dirPath .. '/' .. im.source)
			local newImg = imageSrc:loadImageFile(path, path)

			--[[if(path && (self.imageSrc[path] == nil)){
				newImg = new Image();
				newImg.src = './pstmp/resource/maps/'+path;
				self.imageSrc[path] = newImg;
			}
			else{
				newImg = self.imageSrc[path];
			}]]--
		
			imWidth = im.width --parseInt(im.getAttribute('width'));
			imHeight = im.height --parseInt(im.getAttribute('height'));
			local firstGid = ts.firstgid --parseInt(ts.getAttribute('firstgid'));
		
			--//load/parse the tiles themselves
			local ix
			local iy
		
			local pitch = imWidth/tsWidth
		
			--for(ix = 0; ix < (imWidth/tsWidth); ix++){
			for ix = 0,pitch-1 do
		
				--for(iy = 0; iy < (imHeight/tsHeight); iy++){
				for iy = 0,(imHeight/tsHeight)-1 do
			
					local id = (pitch * iy) + ix + firstGid
				
					local tileObj = {}
					tileObj.img = newImg
					tileObj.x = ix * tsWidth
					tileObj.y = iy * tsHeight
					tileObj.w = tsWidth
					tileObj.h = tsHeight
					tileObj.q = love.graphics.newQuad(tileObj.x, tileObj.y, tileObj.w, tileObj.h, newImg:getWidth(), newImg:getHeight())
				
					tileSrc[id] = tileObj

					--diagPrintTable(tileSrc[id])
			
				end
		
			end
		
			--//Set tile properties
			local tileProps = ts.tile --ts.getElementsByTagName('tile');
		
			--diagPrintTable(ts)

			--for(tpX = 0; tpX < tileProps.length; tpX++){
			--for tpX = 1,#tileProps do
			--	local tp = tileProps[tpX]
			if(type(tileProps) == 'table') then
				for k,tp in pairs(tileProps) do
					if(type(tp) == 'table') then
						local id = tp.id + firstGid
			
						local propElement = tp.properties
						local props = propElement.property

						-- Make it loopable if it ain't
						--[[if(props.name ~= nil) then
							props = {}
							table.insert(props, propElement.property)
						end]]--
			
						--for(pX = 0; pX < props.length; pX++){
						for k,p in pairs(props) do
							if(type(p) == 'table') then
								local pName = p.name
								local pVal = tonumber(p.value) or p.value
				
								tileSrc[id][pName] = pVal
							end
						end
					end
				end
			end
		--//window.alert(firstGid);	
		end
	end
		
	--[[for(i = 1; i <= 32; i++){
		local ot = tileSrc[i];
		
		local outStr = "TileID: "+i+"\n";
		outStr += "TileImg: "+ot.img+"\n";
		outStr += "x: "+ot.x+"\n";
		outStr += "y: "+ot.y+"\n";
		outStr += "w: "+ot.w+"\n";
		outStr += "h: "+ot.h+"\n";
		outStr += "coll: "+ot.coll+"\n";
		//window.alert(outStr);
	}]]--
	
	--// Load layers
	local layerArray = doc.layer --self.doc.getElementsByTagName('layer');
	if(layerArray.name ~= nil) then
		layerArray = {}
		table.insert(layerArray, doc.layer)
	end
	
	--for(lx = 0; lx < layerArray.length; lx++){
	for k,ly in pairs(layerArray) do
		if(type(ly) == 'table') then
			--local ly = layerArray[lx];
		
			local ltag = ly.name --ly.getAttribute('name');
		
			local tmpLayer = {}
		
			tmpLayer.w = ly.width --parseInt(ly.getAttribute('width'));
			tmpLayer.h = ly.height --parseInt(ly.getAttribute('height'));
		
			tmpLayer.data = {}
		
			tmpLayer.lzx = 0
			tmpLayer.lzy = 0
			tmpLayer.dirty = true
		
			local tlArray = nil

			if(type(ly.data) == 'string') then
				tlArray = strToTable(ly.data, ',')
			else
				tlArray = ly.data.tile
			end

			--for(tlx = 0; tlx < tlArray.length; tlx++){
			for tlx = 1,#tlArray do
				local tl = tlArray[tlx]

				local gid = tl
			
				if(type(gid) == 'table') then
					gid = tl.gid
				end
			
				tmpLayer.data[tlx-1] = tileSrc[gid]
			end
		
			--[[tmpLayer.canvas = document.createElement('canvas');
			tmpLayer.canvas.width = 640;
			tmpLayer.canvas.height = 480;
			tmpLayer.ctx = tmpLayer.canvas.getContext('2d');]]--
		
			nt.layer[ltag] = tmpLayer
		
			--//window.alert(ltag);
		end
	end

	return nt
	
end

--[[function TileMap:drawLayer(ctx, layerName, x, y)
	
	local ly = self.layer[layerName]
	
	local xDiff = math.abs(x - ly.lzx)
	local yDiff = math.abs(y - ly.lzy)
	
	if(xDiff > 10 or yDiff > 7 or ly.dirty)
		ly.lzx = Math.floor(x/10) * 10;
		ly.lzy = Math.floor(y/7) * 7;
		//$('output').innerHTML += x+':'+y+'; '+xDiff+'; '+yDiff+'; '+'; '+ly.dirty+'<br />';
		self.renderLayer(layerName);
	}
	
	//ctx.drawImage(ly.canvas, pixels(x-(ly.lzx-10)),pixels(y-(ly.lzy-7)), 320,240, 0,0, 320,240);
	ctx.drawImage(ly.canvas, pixels(x-(ly.lzx-10)),pixels(y-(ly.lzy-7)), 320,240, 0,0, 320,240);
}]]--

-- TODO: Reimplement spriteBatches for this operation, for rendering efficiency.
function TileMap:renderLayer(layerName, buffer, lzx, lzy)

	local hw = units(gconf.vpWidth/2)
	local hh = units(gconf.vpHeight/2)

	local ly = self.layer[layerName]

	local x = (lzx or ly.lzx) - math.floor(hw)
	local y = (lzy or ly.lzy) - math.floor(hh)
	
	local minx = math.max(0,x)
	local maxx = math.min(x + (hw * 4), ly.w)
	local miny = math.max(0,y)
	local maxy = math.min(y + (hh * 4), ly.h)
	
	local buf = buffer or ly.ctx

	local spbs = {}
	
	love.graphics.push()

	love.graphics.setCanvas(buffer)

	if(true or ly.dirty) then

		--buf.clearRect ( 0 , 0 , ly.canvas.width, ly.canvas.height );
		love.graphics.setColor(0,0,0,0)		
		love.graphics.clear()
		love.graphics.setColor(255,255,255,255)
		--buffer:clear()

		--for(iy = miny; iy < maxy; iy++){
		for iy = miny,maxy - 1 do
		
			--for(ix = minx; ix < maxx; ix++){
			for ix = minx, maxx - 1 do
			
				local dx = pixels(ix-x)
				local dy = pixels(iy-y)
				
				local index = ((iy * ly.w) + ix)
				
				local tile = ly.data[index]

				if(tile ~= nil) then
					if(spbs[tile.img] == nil) then
						spbs[tile.img] = self.imageSrc:getSpb(tile.img)
						spbs[tile.img]:clear()
					end

					spbs[tile.img]:add(tile.q, dx, dy, 0,1,1)
					--love.graphics.draw(tile.img, tile.q, dx, dy)
				end
			
			end
		
		end
		ly.dirty = false
		for k,v in pairs(spbs) do
			love.graphics.draw(v, 0, 0, 0, 1, 1, 0, 0)
			v:clear()
		end
	end

	love.graphics.pop()	
	--//ctx.drawImage(ly.canvas, 0,0, 320,240, 0,0, 320,240);
end

function TileMap:getTile(layerName, x, y)

	local ly = self.layer[layerName]
	
	--// layer not found
	if(ly == nil) then return nil end
		
	--// out of bounds
	if(x < 0 or x >= ly.w or y < 0 or y >= ly.h) then return nil end
	
	local index = (y * ly.w) + x
	
	return ly.data[index]

end
