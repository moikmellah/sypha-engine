--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

Collision = {}

function Collision.new(obj, caller)

	local nColl = {}

	nColl.obj = obj
	nColl.ttl = 0;
	nColl.stale = false;

	return nColl

end
