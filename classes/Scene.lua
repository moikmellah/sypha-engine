--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')
require('classes.Actor')
require('classes.Camera')
require('classes.ImageLibrary')
require('classes.InputLibrary')
require('classes.ActionLibrary')
require('classes.ActorLibrary')
require('classes.TextLibrary')
require('classes.QuadCell')
require('classes.TileMap')

-- Table declaration for class
Scene = {}

-- Metatable for class (to make Scene table the default lookup for class methods)
Scene_mt = {}
Scene_mt.__index = Scene

function Scene.new(s_file, env)

	local nScene = {}	

	setmetatable(nScene, Scene_mt)

	local fPath,dirPath = pathToFile(s_file)
	nScene.relPath = dirPath

	----debugPrint(fPath,2)

	nScene.config = Xml.deserializeFile(fPath) --loadXmlAsTable(s_file);
	nScene.env = env
	nScene.sceneFile = s_file

	--diagPrintTable(nScene.config)

	return nScene

end

function Scene:init()

	if(self.env ~= nil) then
		self.imgLib = self.env.imgLib
	end
	--//Set imageLib, if not already done
	if(self.imgLib == nil) then self.imgLib = ImageLibrary.new() end

	self.tilemap = self.tilemap or {}

	--//Load map (only one?  May need further work.)
	if(self.config.tilemap) then
		for m = 0,#self.config.tilemap do
			local fPath = pathToFile(self.relPath .. '/' .. self.config.tilemap[m])
			--debugPrint(fPath)
			self.tilemap[m] = TileMap.new(fPath, self.imgLib, self.tileLib)
		end
	end
	
	self.mousePos = {x = 0, y = 0}
	
	self.collisionRoot = QuadCell.new(nil, 0, 0, QuadCell.minSize * 4, QuadCell.minSize * 4)

	self.camera = {}
	self.actorList = {}
	self.actorPrio = {}
	self.actorRemove = {}
	
	self.defaultCam = {}
	self.defaultCam.x = units(gconf.vpWidth/2)
	self.defaultCam.y = units(gconf.vpHeight/2)

	self.eventQueue = {}

	self.vars = self.config.vars or {}
	
	self.sortTimer = 100
	self.sortThreshold = 0.25
	
	--for(local k = 0; k < self.config.camera.length; k++){
	for k,cam in pairs(self.config.camera) do
		--local cam = self.config.camera[k];
		
		if(type(cam) == 'table') then

			local camObj = nil;

			if(cam.init ~= 0) then
		
				camObj = self:spawn(cam, cam.x, cam.y, cam.z)
			
				camObj.ctx = self.env.camCanvas[k] or love.graphics.newCanvas(gconf.vpWidth*2, gconf.vpHeight*2)
				self.env.camCanvas[k] = camObj.ctx
				camObj.ctx:setFilter('linear', 'nearest')
				camObj.lzx = 0
				camObj.lzy = 0
				camObj.dirty = true
			
				camObj.ctx2 = camObj.ctx --love.graphics.newCanvas(gconf.vpWidth*2, gconf.vpHeight*2)
				camObj.ctx2:setFilter('linear', 'nearest')
				camObj.lzx2 = 0;
				camObj.lzy2 = 0;
			
				self.camera[k] = camObj
			end
			--//window.alert('inst cam '+k)
		end
	end
	
	if(self.camera[0] ~= nil) then 
		self.mousePos.x = self.camera[0].x 
		self.mousePos.y = self.camera[0].y
	end
	
	-- This nesting is ridiculous.  Fix it.
	if(self.tilemap[0] ~= nil) then
		for m = 0,#self.tilemap do

			local t = self.tilemap[m]

			local ol = t.objectgroup

			if(type(ol) == 'table') then

				for k,v in pairs(ol) do

					if(type(v) == 'table' and type(v.object) == 'table') then

						for ok,ov in pairs(v.object) do

							if(type(ov) == 'table') then
								local nx = units(ov.x)
								local ny = units(ov.y)
								local parms = {w = units(ov.width), h = units(ov.height)}
								parms.tiledObjName = ov.name
								if(ov.properties ~= nil and ov.properties.property ~= nil) then
									for k,v in pairs(ov.properties.property) do
										if(type(v) == 'table') then parms[k] = v.value end
									end
								end
								local ntype = t.tileSrc[ov.gid].actorType
								if(type(ntype) == 'number') then ntype = self.config.actor[ntype].template end
								local newObj = self:spawn(ntype, nx, ny, nil, parms)
								if(newObj.hero == 1) then self.hero = newObj end

							end

						end

					end

				end

			end

		end
	end

	--Finally, import outside objs
	while #self.env.importActors > 0 do
		local a = table.remove(self.env.importActors)
		if(a.toDoor ~= nil) then
			local dQuery = Queryable.new(self.actorList
				):First('x => x.objType == "exit" and x.tiledObjName == "exit_'..a.toDoor..'"')
			local offset = 3
			if(string.match(a.toDoor, 'e$')) then offset = -3 end
			self:import(a.obj, dQuery.x + offset, dQuery.y - .1, dQuery.z)
		else
			self:import(a.obj, a.x, a.y, a.z)
		end
		if(a.obj.hero == 1) then self.hero = a.obj end
	end
	
	-- FINALLY finally, run the scene's initscript, if present.
	if(self.initscript == nil and type(self.config.initscript) == 'string') then
		self.initscript = loadstring('local self = ... '.. self.config.initscript)
	end

	if(type(self.initscript) == 'function') then
		self:initscript()
	end

	--[[if(false and self.config.vars.gobs) then
	{
		for(local j = 0; j < self.config.vars.gobs; j++) self.spawn(self.config.actor[27], 0, 42.999, Math.random()*2.8-1.4);
	}]]--
	
end

function Scene:unloadObjs()

	--debugPrint('Unloading ' .. #self.actorPrio .. ' objs from scene.', 3)

	for k,v in pairs(self.actorPrio) do
		self:remove(v)
	end
	self:purgeRemovedObjs()
	
end

function Scene:spawn(template, f_x, f_y, f_z, t_parms)

	local x = f_x or 0
	local y = f_y or 0
	local z = f_z

	local tmpTab = nil

	if(type(template) == 'table') then
		tmpTab = template
	elseif(type(template) == 'string') then
		tmpTab = self.env.actorLib.actors[template]
	end
	--/* else get from ActorLib */

	local engClass = 'Actor'
	
	if(type(tmpTab) == 'table') then
		engClass = tmpTab.engineClass or 'Actor'
	end
	
	local instClass = _G[engClass]
	
	local obj = instClass:new(tmpTab, self, t_parms)
	
	obj.x = x
	obj.y = y
	obj.z = z or obj.z or 0
	obj.input = self.input
	obj.eventPos = #self.eventQueue
	
	obj.camera = obj.camera or self.vars.defaultCamera or 0
	if(self.camera[obj.camera] == nil) then
		obj.camera = self.vars.defaultCamera
	end

	--self.actorList.push(obj)
	table.insert(self.actorList, obj)
	--self.actorPrio.push(obj)
	table.insert(self.actorPrio, obj)
	if(obj.noColl ~= 1) then self.collisionRoot:place(obj) end
	
	self.forceSort = true

	return obj
	
end

function Scene:import(template, f_x, f_y, f_z)

	local x = f_x or 0
	local y = f_y or 0
	local z = f_z or 0

	if(type(template) ~= 'table') then
		return false
	end
	--/* else get from ActorLib */
	
	local obj = template --Actor:new(tmpTab, self)

	obj.x = x
	obj.y = y
	obj.z = z
	obj.input = self.input
	obj.eventPos = #self.eventQueue

	obj.scene = self
	obj.refs.helper = nil
	obj.refs.collCell = nil
	obj.refs.collisions = {}
	obj.refs.collPhysArray = nil
	
	obj.camera = obj.camera or self.vars.defaultCamera or 0

	-- YAY KLUDGE
	obj.timer.parent.children[obj.timer] = obj.timer

	table.insert(self.actorList, obj)
	table.insert(self.actorPrio, obj)
	if(obj.noColl ~= 1) then self.collisionRoot:place(obj) end
	
	self.forceSort = true

	return obj
	
end

function Scene:update(dt)

	self.sortTimer = self.sortTimer + dt

	QuadCell.colls = 0
	QuadCell.checks = 0
	
	local k = 1
	for k = 1,#self.actorPrio do
		self.actorPrio[k]:update()
	end

	for k = 1,#self.actorPrio do
		self.actorPrio[k].refs.gndObj = nil
		self.actorPrio[k]:getBounds(true)
	end

	self:purgeRemovedObjs()

	self.collisionRoot = self.collisionRoot:getRoot()
	self.collisionRoot:shakeDown()
	self.collisionRoot = self.collisionRoot:getRoot()
	self.collisionRoot:expireColls()
	self.collisionRoot:doColls()
	self.collisionRoot:removeColls()

	self.collisionRoot:doMaint(dt)

	for k = 1,#self.actorPrio do
		self.actorPrio[k]:collPhysics()
		if(self.actorPrio[k].doinertia == 1) then
			self.actorPrio[k]:applyForces()
		end
	end

end

function Scene:draw(ctx, ctx2)
	
	if(self.sortTimer > self.sortThreshold or self.forceSort == true) then
		table.sort(self.actorList, objRenderSort)
		table.sort(self.actorPrio, objPrioSort)
		self.sortTimer = 0
		self.forceSort = false
	end
	
	for i = 1,#self.actorList do
		local obj = self.actorList[i]
		
		local objCam = obj.camera or 0
		
		local cam = self.camera[objCam] or self.camera[self.vars.defaultCamera]
		
		obj:draw(ctx, ctx2, cam.x, cam.y)
		
	end

	local fx = 20
	local d = 3
	if(self.env.vars.reverseStereo == true) then d = -d end

	-- Don't adjust depth if we're not rendering in stereo
	if(ctx2 == nil) then d = 0 end


end

function Scene:clickHandle(x,y)

	local cam = self.camera[0]
	
	cam.xt = cam.x + (x - units(gconf.vpWidth/2))
	cam.yt = cam.y + (y - units(gconf.vpHeight/2))

	return true
end

function Scene:getTile(camId, x, y)
	
	local cam = self.camera[camId]
	
	if(cam == nil or cam.map == nil or cam.layer == nil or self.tilemap[cam.map] == nil) then return nil end
	
	return self.tilemap[cam.map]:getTile(cam.layer, x, y)
	
end

function Scene:pushEvent(ev)

	if(type(ev) ~= 'table') then return false end

	local subj = 'anonymous'

	if(ev.subj ~= nil) then subj = ev.subj.actionSet end

	--debugPrint('Event sent: ' .. ev.type .. ' by ' .. subj)

	table.insert(self.eventQueue, ev)

	return true

end

function Scene:remove(obj)

	table.insert(self.actorRemove, obj)

end

function Scene:purgeRemovedObjs()

	local obj = table.remove(self.actorRemove)

	while (obj ~= nil) do

		obj.timer.parent:removeChild(obj.timer)

		for k = #self.actorList,1,-1 do
			if(self.actorList[k] == obj) then
				table.remove(self.actorList, k)
				break
			end
		end

		for k = #self.actorPrio,1,-1 do
			if(self.actorPrio[k] == obj) then
				table.remove(self.actorPrio, k)
				break
			end
		end

		if(obj.refs.collCell ~= nil) then
			obj.refs.collCell:removeObj(obj)
		end

		obj = table.remove(self.actorRemove)
	end

end

function Scene:getExit(id)

	local e = self.config.exit[id]

	return e.x, e.y, e.z, pathToFile(self.relPath .. '/' .. e.scene)

end

function Scene:setFullscreen(mode)

	if(mode) then
		love.window.setMode(0,0,{fullscreen=true})
		love.mouse.setVisible(false)
	else
		love.window.setMode(gconf.vpWidth*2,gconf.vpHeight*2,{fullscreen=false, resizable=true})
		love.mouse.setVisible(true)
	end

	if(self.camera) then
		for k,v in pairs(self.camera) do
			v.dirty = true
		end
	end

end
