--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
DeltaTimer = {}

-- Metatable for class (to make DeltaTimer table the default lookup for class methods)
DeltaTimer_mt = {}
DeltaTimer_mt.__index = DeltaTimer

-- Constructor
function DeltaTimer.new(t_parent)

	-- Make copy from template if provided, or default template if not
	local nActLib = {}

	-- Set metatable to get our class methods
	setmetatable(nActLib, DeltaTimer_mt)

	nActLib.dtFactor = 1
	nActLib.currDt = 0
	nActLib.totalDt = 0
	nActLib.len = nil
	nActLib.children = {}
	nActLib.parent = t_parent
	if(type(t_parent) == 'table') then
		t_parent.children = t_parent.children or {}
		--table.insert(t_parent.children, nActLib)
		t_parent.children[nActLib] = nActLib
		nActLib:add(t_parent:dt())
	end

	return nActLib

end

function DeltaTimer:removeChild(child)
	self.children[child] = nil
end

function DeltaTimer:removeFromParent()
	if(self.parent ~= nil) then
		self.parent:removeChild(self)
	end
	self.parent = nil
end

function DeltaTimer:setParent(t_parent)
	self:removeFromParent()
	if(type(t_parent) ~= 'table') then return end
	self.parent = t_parent
	self.parent.children[self] = self
end

function DeltaTimer:dtMult()

	local dtFactor = self.dtFactor

	if(self.parent ~= nil) then
		dtFactor = dtFactor * self.parent:dtMult()
	end

	if(dtFactor < 0) then dtFactor = 0 end

	return dtFactor

end

function DeltaTimer:set(dtFct, len)
	local d = dtFct or 1
	
	if(d < 0) then d = 1 end

	self.dtFactor = d
	self.len = len

end

function DeltaTimer:add(dt)

	local d = dt or 1

	self.currDt = d * self.dtFactor
	
	self.totalDt = (self.totalDt or 0) + self.currDt

	for k,v in pairs(self.children) do
		v:add(self.currDt)
	end

	if(type(self.len) == 'number') then 
		self.len = self.len - dt
		if(self.len <= 0) then
			self.len = nil
			self.dtFactor = 1
		end
	end

end

function DeltaTimer:dt(tag)
	return self.currDt
end
