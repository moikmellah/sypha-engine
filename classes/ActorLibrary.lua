--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
ActorLibrary = {}

-- Metatable for class (to make ActorLibrary table the default lookup for class methods)
ActorLibrary_mt = {}
ActorLibrary_mt.__index = ActorLibrary

-- Constructor
function ActorLibrary:init()

	-- Make copy from template if provided, or default template if not
	if(self.actors == nil) then self.actors = {} end

end


function ActorLibrary:loadSet(s_file)

	self.actors = self.actors or {}

	local loadTab = Xml.deserializeFile(s_file)
	--loadXmlAsTable(s_file);
	
	tableMerge(self.actors, loadTab.actor)
	
end
