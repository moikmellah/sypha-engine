--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
AudioLibrary = {}

-- Metatable for class (to make AudioLibrary table the default lookup for class methods)
AudioLibrary_mt = {}
AudioLibrary_mt.__index = AudioLibrary

AudioLibrary.cloneLimit = 10

-- Constructor
function AudioLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nImgLib = {}

	-- Set metatable to get our class methods
	setmetatable(nImgLib, AudioLibrary_mt)

	nImgLib.sounds = {}

	--nImgLib.imageSets = {}

	nImgLib.count = 0

	nImgLib.clock = 0
	nImgLib.dtThreshold = 0

	return nImgLib

end

function AudioLibrary:loadAudioFile(id, filePath, instLimit)

	local iLimit = instLimit or 10

	local sType = 'static'

	if(instLimit == 1) then
		sType = 'stream'
	end

	if self.sounds == nil then 
		self.sounds = {} 
	end

	self.count = self.count or 0

	-- Already loaded.  Tank.
	if self.sounds[id] ~= nil then return self.sounds[id].source end

	local tempSnd = love.audio.newSource(filePath, sType)

	if tempSnd ~= nil then 
		self.sounds[id] = {}
		self.sounds[id].source = tempSnd
		self.sounds[id].inst = {}
		self.sounds[id].limit = iLimit
		self.sounds[id].inst[1] = tempSnd:clone()
		--self.sounds[id].last = 0
	end

	return self.sounds[id]

end

function AudioLibrary:play(id, loop)

	local snd = self.sounds[id]

	if not(snd) then return nil end

	-- Get first non-playing instance
	local inst = self:findAvailable(id)

	if not(inst) then return nil end

	inst:setLooping(loop == true)

	inst:play()

	return inst

end

function AudioLibrary:playBgm(id)
	if(id ~= self.curBgm) then
		self:stopAll()
		self:play(id, true)
		self.curBgm = id
	end
end

function AudioLibrary:findAvailable(id)

	local snd = self.sounds[id]

	if not(snd) then return nil end

	if((snd.last or -1) >= self.clock - self.dtThreshold) then return nil end

	local inst = nil

	local c = 1

	while c <= #snd.inst and not(inst) do
		if not(snd.inst[c]:isPlaying()) then
			inst = snd.inst[c]
		end
		c = c + 1
	end

	if not(inst) and #snd.inst < snd.limit then
		inst = snd.source:clone()
		snd.inst[#snd.inst + 1] = inst
	end

	snd.last = self.clock

	return inst

end

function AudioLibrary:stopAll()

	self.sounds = self.sounds or {}

	for k,v in pairs(self.sounds) do

		for c,i in ipairs(v.inst) do

			i:stop()
			love.audio.rewind(i)

		end

	end

end

function AudioLibrary:setMaster(vol)

	self.master = vol

	love.audio.setVolume(self.master)

end
