--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
TilesetLibrary = {}

-- Metatable for class (to make TilesetLibrary table the default lookup for class methods)
TilesetLibrary_mt = {}
TilesetLibrary_mt.__index = TilesetLibrary

-- Constructor
function TilesetLibrary:init(imgLib)

	self.tilesets = self.tilesets or {}

	self.imageLib = imgLib or ImageLibrary.new()

end


function TilesetLibrary:loadSet(filePath)

	self.tilesets = self.tilesets or {}

	local s_file,dirPath = pathToFile(filePath)

	if(type(self.tilesets[s_file]) == 'table') then
		return self.tilesets[s_file]
	end

	--debugPrint(s_file)

	local base = Xml.deserializeFile(s_file)

	for k,ts in pairs(base) do

		----debugPrintTable(ts)

		--local ts = tileSets[tsX];
		if(type(ts) == 'table') then
			local tmpts = {}
	
			tmpts.tiles = {}

			local tileSrc = tmpts.tiles
			local imageSrc = self.imageLib

			local tsName = ts.name --ts.getAttribute('name');
	
			local imWidth
			local imHeight
	
			local tsWidth = ts.tilewidth --parseInt(ts.getAttribute('tilewidth'));
			local tsHeight = ts.tileheight --parseInt(ts.getAttribute('tileheight'));

			-- Should only be one for a given TileSet
			--local imageList = ts.image --ts.getElementsByTagName('image');
	
			--//load any referenced images
			local im = ts.image
	
			local path = pathToFile(dirPath .. '/' .. im.source)
			local newImg = imageSrc:loadImageFile(path, path)

			imWidth = im.width --parseInt(im.getAttribute('width'));
			imHeight = im.height --parseInt(im.getAttribute('height'));
	
			--//load/parse the tiles themselves
			local ix
			local iy
	
			local pitch = imWidth/tsWidth
	
			--for(ix = 0; ix < (imWidth/tsWidth); ix++){
			for ix = 0,pitch-1 do
	
				--for(iy = 0; iy < (imHeight/tsHeight); iy++){
				for iy = 0,(imHeight/tsHeight)-1 do
		
					local id = (pitch * iy) + ix
			
					local tileObj = {}
					tileObj.img = newImg
					tileObj.x = ix * tsWidth
					tileObj.y = iy * tsHeight
					tileObj.w = tsWidth
					tileObj.h = tsHeight
					tileObj.q = love.graphics.newQuad(tileObj.x, tileObj.y, tileObj.w, tileObj.h, newImg:getWidth(), newImg:getHeight())
			
					tileSrc[id] = tileObj

					--diagPrintTable(tileSrc[id])
		
				end
	
			end
	
			--//Set tile properties
			local tileProps = ts.tile --ts.getElementsByTagName('tile');
	
			if(type(tileProps) == 'table') then
				for k,tp in pairs(tileProps) do
					if(type(tp) == 'table') then
						local id = tp.id
		
						local propElement = tp.properties
						local props = propElement.property

						for k,p in pairs(props) do
							if(type(p) == 'table') then
								local pName = p.name
								local pVal = tonumber(p.value) or p.value
			
								tileSrc[id][pName] = pVal
							end
						end
					end
				end
			end
			self.tilesets[s_file] = tmpts
		end

	end

	return self.tilesets[s_file]
	
end
