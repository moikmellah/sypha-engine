--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
ActionLibrary = {}

-- Metatable for class (to make ActionLibrary table the default lookup for class methods)
ActionLibrary_mt = {}
ActionLibrary_mt.__index = ActionLibrary

-- Constructor
function ActionLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nActLib = {}

	-- Set metatable to get our class methods
	setmetatable(nActLib, ActionLibrary_mt)

	nActLib.actionSet = {}
	nActLib.count = 0

	return nActLib

end

function ActionLibrary:init()

	if self.actionSet == nil then self.actionSet = {} self.count = 0 end

end

function ActionLibrary:loadSet(s_fileName)

	--if(s_handle == nil) then return false end

	self:init()

	if(s_fileName == nil) then return false end

	if self.actionSet == nil then self.actionSet = {} end

	--if self.actionSet[handle] ~= nil then return true end

	local aS = Xml.deserializeFile(s_fileName)
	--print(Xml.serialize(aS))
	--self.actionSet[s_handle] = aS
	for k,v in pairs(aS.actionSet) do
		self.actionSet[k] = v
	end

end

