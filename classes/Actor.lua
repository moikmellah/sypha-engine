--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('classes.ImageLibrary')
require('utils.Inheritance')
require('classes.Collision')

-- Member fields template; used if no other template is given
ActorDefaults = {

	x = 0,				-- Now handled in 'units', NOT px (1 unit == 16 px, currently)
	y = 0,
--	visible = 0,			-- Still relevant
	imageSet = 'advnt',			-- Keep this; Images will be stored in an array from now on.
	actionSet = nil,
	facing = 1,
	lrInput = {forward = 'right', back = 'left'},			-- Maps left/right to forward/backward, depending on facing.
	blendfx = {			-- Possibly deprecated.  We'll see.
		fxmode = 0,
		alpha = 1,
		color = 0xffffffff
	},
	label = {			-- Also possibly deprecated.  At least, needs revamp.
		visible = 0,	-- visibility
		x = 0,		-- offset from obj pos
		y = 0,		-- offset from obj pos
		fontSet = 0,	-- imageSet used to store glyphs (obsolete, with Love2d)
		iMode = 0,	-- blending mode (0-3 only) (also obsolete)
		hMode = 0,	-- horizontal alignment mode (0-3 only) (Possibly still relevant.) (Nope.)
		alpha = 1,	-- alpha value for blending (0.0 to 1.0)
		color = 0xffffffff	-- RGBA; A is alpha value for color blending, though usage depends on iMode
	},
	prim = {
		visible = 0,	-- visibility
		x = 0,		-- offset from obj pos
		y = 0,		-- offset from obj pos
		width = 0,
		height = 0,
		color = 0
	},
	cam = 1,			-- Background to which we're affine (determines rendering coordinates)
					-- This needs a rework.  Possibly a tree from now on?
	
	parent = nil,			-- This will typically be the object which spawned this one.
	children = nil,			-- List of all objects spawned by this one

	luaClass = "GameObject",	-- These stick around.  They cool.
	class = "GameObject",
	name = "Object"

}

helperTemplate = {x = 0, y = 0, actionSet = nil, currentAction = nil, currentFrame = nil, imageSet = nil}

-- Table declaration for class
Actor = Inherit(nil, Serializable)

-- Metatable for class (to make GameObject table the default lookup for class methods)
Actor_mt = {}
Actor_mt.__index = Actor

Actor.DIRBITS = {up = 1, down = 2, left = 4, right = 8}

-- Constructor
function Actor:new(template,parentScene,t_parms)

	-- Template ref
	local gTmp = nil

	-- Check if arg is a table; if not, use the default table declared above
	if type(template) == "table" then
		gTmp = template
	else
		gTmp = ActorDefaults
	end

	-- Make copy from template if provided, or default template if not
	local gObj = tableCopy(gTmp)
	
	if(type(t_parms) == 'table') then tableMerge(gObj, t_parms, true) end

	local clsMt = {}
	clsMt.__index = self or Actor
	
	-- Set metatable to get our class methods
	setmetatable(gObj, clsMt)

	gObj.class = gObj.engineClass or "Actor"

	gObj.vars = gObj.vars or {}
	gObj.refs = gObj.refs or {}
	gObj.refs.collisions = gObj.refs.collisions or {}
	gObj.scene = parentScene
	gObj.label = gObj.label or {x=0, y=0, gfx={r=255, g=255, b=255, a=255}, visible=false}
	gObj.refs.eventQueue = {}
	gObj.refs.eventQueueHead = 1
	gObj.refs.eventQueueTail = 1
	local pTimer = nil
	if(parentScene ~= nil) then pTimer = parentScene.env.timers[gObj.vars.timer or 'master'] end
	gObj.timer = DeltaTimer.new(pTimer)

	local actSet = gObj:getActionSet()

	if(actSet ~= nil) then
		local initScript = actSet.initscript
		if(type(actSet.initscript) == 'string') then
			actSet.initscript = loadstring('local self,that = ... '.. actSet.initscript)
		end

		if(type(actSet.initscript) == 'function') then
			gObj.tmpScr = actSet.initscript
			gObj:tmpScr()
		end
	end

	-- Return object ref
	return gObj

end

function Actor:setCollision(obj)

	if(obj == null) then return false end

	self.refs.collisions = self.refs.collisions or {}
	
	local coll = self.refs.collisions[obj]
	
	if(coll == nil) then
		coll = Collision.new(obj, self)
		self.refs.collisions[obj] = coll
	end
	
	coll.stale = false
	coll.zone = coll.zone or {}
	coll.zone[0],coll.zone[1],coll.zone[2],coll.zone[3] = self:getOverlap(obj)
	
end

function Actor:expireColls()

	for k,v in pairs(self.refs.collisions) do
		v.stale = true
		v.phys = nil
	end

end

function Actor:removeColls()

	local c = self.refs.collisions
	
	for k,v in pairs(self.refs.collisions) do
		if(v.stale) then c[k] = nil end
	end

end

function Actor:getActionSet()

	if(self.actionSet ~= nil and self.scene ~= nil) then 
		self.refs.actionSet = self.refs.actionSet or self.scene.actionLib.actionSet[self.actionSet]
	end
	
	return self.refs.actionSet
	
end

function Actor:getAction()

	local actSet = self.refs.actionSet or self:getActionSet()
	if(actSet ~= nil) then 
		self.refs.action = self.refs.action or actSet.action[self.action] 
	end
	
	return self.refs.action
	
end

function Actor:getFrame()

	local act = self.refs.action or self:getAction()
	if(act ~= nil) then 
		self.refs.frame = self.refs.frame or act.frame[self.frame] 
	end
	
	return self.refs.frame

end

function Actor:getCell()

	local frm = self.refs.frame or self:getFrame()

	if(frm ~= nil and frm.quad ~= nil and self.imageSet ~= 'NONE') then
		local qn = frm.quad
		self.refs.cell = self.refs.cell or self.scene.imgLib:getCell(self.imageSet, qn)
	end

	return self.refs.cell

end

function Actor:getImage()
	if(self.scene ~= nil) then
		return self.scene.imgLib.images[self.imageSet].img
	end
end

function Actor:update()

	self.refs.actionSet = nil
	self.refs.action = nil
	self.refs.frame = nil
	self.refs.cell = nil

	if(self.bnd ~= nil) then self.bnd[4] = false end

	local dt = self.timer:dt()
	
	self:doAction(dt)
	if(self.dophysics == 1) then
		self:move(dt)
	else
		self:moveSimple(dt)
	end

end

function Actor:getX()

	if(self.position == 'parent' and self.refs.parentObj ~= nil) then
	
		local p = self.refs.parentObj
		local ox = p:getX() or 0
		return (self.x * (self.facing or 1)) + ox

	elseif(self.position == 'parentHand' and self.refs.parentObj ~= nil) then
	
		local p = self.refs.parentObj
		local cell = p:getCell()
		local ox = p:getX() or 0
		if(cell ~= nil and cell.hofs ~= nil) then
			ox = ox + units(cell.hofs.x * p.facing * (p.sx or 1))
		end
		return ox + (self.x * (self.facing or 1) * (self.sx or 1))

	end
	
	return self.x

end

function Actor:getHandX()

	local myCell = self:getCell()
	if(myCell ~= nil and myCell.hofs ~= nil) then
		return self:getX() + units(myCell.hofs.x * (self.facing or 1) * (self.xs or 1))
	end

	return self:getX()

end

function Actor:getY()

	if(self.position == 'parent' and self.refs.parentObj ~= nil) then
	
		local p = self.refs.parentObj
		local oy = p:getY() or 0
		return self.y + oy
	
	elseif(self.position == 'parentHand' and self.refs.parentObj ~= nil) then
	
		local p = self.refs.parentObj
		local cell = p:getCell()
		local oy = p:getY() or 0
		if(cell ~= nil and cell.hofs ~= nil) then
			oy = oy + units(cell.hofs.y * (p.sy or 1))
		end
		return (self.y * (self.ys or 1)) + oy
	end
	
	return self.y

end

function Actor:getHandY()

	local myCell = self:getCell()
	if(myCell ~= nil and myCell.hofs ~= nil) then
		return self:getY() + units(myCell.hofs.y * (self.ys or 1))
	end

	return self:getY()

end

function Actor:draw(ctx, ctx2, f_x, f_y)

	local camx = f_x or 0
	local camy = f_y or 0
	
	local m = math

	local sx1 = pixels(camx - units(gconf.vpWidth/2))
	local sy1 = pixels(camy - units(gconf.vpHeight/2))
	local sx2 = pixels(camx + units(gconf.vpWidth/2))
	local sy2 = pixels(camy + units(gconf.vpHeight/2))
	
	local orgx = pixels(self:getX())
	local orgy = pixels(self:getY())

	local sm = self.scene.env.vars.stereoMag or 0
	local d = 0 --ctx2 ? -Math.round((this.z || 0)*sm*2)/2 : 0;
	
	if(self.scene.env.vars.reverseStereo) then d = -d end
	
	self.facing = self.facing or 1
	
	local action = self:getAction()

	local cfrm = self.frame

	local myfrm = self:getFrame()

	local sx = (myfrm.sx or 1) * (self.sx or 1)
	local sy = (myfrm.sy or 1) * (self.sy or 1)

	if(self.prim ~= nil) then
	
		--local lbx = orgx - sx1
		local lbx = orgx - sx1
		local lby = orgy - sy1

		for k,lbl in ipairs(self.prim) do
			if(type(lbl) == 'table' and lbl.visible == 1) then
				if(lbl.type == 'ellipse' or lbl.type == 'circle') then
					self.scene.txtLib:renderEllipse(ctx, ctx2, lbl, lbx,lby,0, sx,sy, self.facing)
				elseif(lbl.type == 'line') then
					self.scene.txtLib:renderLine(ctx, ctx2, lbl, lbx,lby,0, sx,sy)
				elseif(lbl.type == 'pointSet') then
					self.scene.txtLib:renderPoints(ctx, ctx2, lbl, lbx,lby,0, sx,sy)
				elseif(lbl.type == 'image') then
					local cl = self.scene.imgLib:getCell(lbl.imageSet, lbl.quad)
					self.scene.txtLib:renderImg(ctx, ctx2, lbl, cl, lbx,lby,0, sx,sy)
				else
					self.scene.txtLib:renderLabel(ctx, ctx2, lbl, lbx,lby,0, sx,sy)
				end



			end
		end

	end

	local cell = self:getCell()

	if ((self.visible ~= 0) and (myfrm.visible ~= 0) and cell ~= nil) then --return false end

		local iq = cell.quad
		local qw = iq.w
		local qh = iq.h
	
		local tx1 = pixels(self:getX()) - (sx * self.facing * cell.origin.x)
		local tx2 = tx1 + (sx * self.facing * qw)
		local ty1 = pixels(self:getY()) - (sy * cell.origin.y)
		local ty2 = ty1 + (sy * qh)

		local ox1 = m.min(tx1,tx2)
		local oy1 = m.min(ty1,ty2)
		local ox2 = m.max(tx1,tx2)
		local oy2 = m.max(ty1,ty2)
	
		if((ox1 < sx2) and (sx1 < ox2) and (oy1 < sy2) and (sy1 < oy2)) then

			if (cell ~= nil and self.scene ~= nil) then

				love.graphics.push()

				local sfc = self.vars.color or myfrm.color

				if(sfc ~= nil) then
					love.graphics.setColor((sfc.r or 1)*255,(sfc.g or 1)*255,(sfc.b or 1)*255,(sfc.a or 1)*255)
					self.scene.txtLib:setShader(sfc.shader)
				end

				love.graphics.setCanvas(ctx)

				love.graphics.drawq(cell.img, cell.gquad, orgx-sx1-d, orgy-sy1, 0, sx * self.facing, sy, cell.origin.x, cell.origin.y)
			
				if(ctx2 ~= nil) then
				
					love.graphics.setCanvas(ctx2)

					love.graphics.drawq(cell.img, cell.gquad, orgx-sx1+d, orgy-sy1, 0, sx * self.facing, sy, cell.origin.x, cell.origin.y)
			
				end

				self.scene.txtLib:setShader()

				love.graphics.setColor(255,255,255,255)

				love.graphics.pop()

			end

		end
	
	end

	if(type(self.vars.boundColor) == 'table') then

		local bc = self.vars.boundColor

		love.graphics.push()
		love.graphics.setCanvas(ctx)

		local rc = self:getBounds(true)

		local bl = pixels(rc[0]) - sx1
		local bt = pixels(rc[1]) - sy1
		local bw = pixels(rc[2] - rc[0])
		local bh = pixels(rc[3] - rc[1])

		love.graphics.setColor((bc.r or 1)*255,(bc.g or 1)*255,(bc.b or 1)*255,(bc.a or 1)*255)

		love.graphics.rectangle('fill', bl,bt, bw,bh)

		love.graphics.setColor(255,255,255,255)

		love.graphics.pop()

	end

end

function Actor:doAction(dt)

	self.dt = dt

	local nxAct = nil
	local nxFrm = nil

	if(self.nxAct ~= nil and self.nxAct ~= self.action) then 
		self.action = self.nxAct 
		self.aTtl = nil 
		self.curTtl = nil
		self.aiTtl = 0
	end
	if(self.nxFrm ~= nil and (tonumber(self.nxFrm) or self.frame) ~= self.frame) then 
		self.frame = tonumber(self.nxFrm) or self.nxFrm 
		self.curTtl = nil
	end

	local myAction = self:getAction()

	local k,v

	local cfrm = self.frame

	local myFrame = self:getFrame()

	local myCell = self:getCell()

	if(myCell ~= nil and myCell.hofs ~= nil and myFrame.hfrm ~= nil) then
		
		if(self.refs.helper == nil and self.vars.nxHelper ~= nil) then
			self.refs.helper = self:spawnHelper(self.vars.nxHelper)
			local ha,hb,hAct,hFrm = string.find(myFrame.hfrm, '^(.+)%.(.+)$')
			self.refs.helper.action = hAct or 'DEFAULT'
			self.refs.helper.frame = tonumber(hFrm) or 1
		else
			local ha,hb,hAct,hFrm = string.find(myFrame.hfrm, '^(.+)%.(.+)$')
			self.refs.helper.nxAct = hAct or 'DEFAULT'
			self.refs.helper.nxFrm = tonumber(hFrm) or 1

		end

	else

		if(self.refs.helper ~= nil) then
			self.scene:remove(self.refs.helper)
			self.refs.helper = nil
		end

	end

	if(self.refs.helper ~= nil and myFrame.hofs ~= nil) then
		self.refs.helper.nxFrm = self.nxFrm
		self.refs.helper.nxAct = self.nxAct
	end

	local ta,tb,tc

	if((type(myAction) == 'table') and (type(myFrame) == 'table')) then
		
		--// First, execute the logic of the frame.
		--[[//for k = 1,#action.frame[cfrm].logic do
			//nxAct,nxFrm = this.tryLogicNode(action.frame[cfrm].logic[k])
			//if(nxAct != null) { break }
		//}]]--

		if(dt > 0 and nxAct == nil) then
			nxAct,nxFrm = self:checkLocalEvents()
		end

		if(dt > 0 and nxAct == nil) then
			nxAct,nxFrm = self:checkSceneEvents()
		end

		if (type(myAction.initscript) == "string") then
			myAction.initscript = loadstring('local self,that = ... '.. myAction.initscript)
		end

		if (nxAct == nil and (type(myAction.initscript) == "function") and (self.aTtl == nil)) then
			self.tmpScr = myAction.initscript
			local retStr = self:tmpScr()		-- Run it on the obj.
			if(type(retStr) == 'string') then
				ta,tb,nxAct,nxFrm = string.find(retStr, '^(.+)%.(.+)$')
			end
			
		end

		--[[local nodeRet = self:tryLogicNode(myFrame, false)
		if(nodeRet) then
			nxAct = nodeRet[1]
			nxFrm = nodeRet[2]
		end]]--
		if (nxAct == nil) then
			nxAct,nxFrm = self:execLogicNode(myFrame, false)
		end

		--// If we aren't breaking out yet, run the logic of the Action.
		if (nxAct == nil) then
			nxAct,nxFrm = self:execLogicNode(myAction, true)
		end

		--// If we aren't breaking out yet, run the logic of the Action.
		if (nxAct == nil) then
			nxAct,nxFrm = self:execLogicNode(self:getActionSet(), true)
		end

				

		--// Apply dt to curTtl
		self.curTtl = (self.curTtl or 0) + dt
		self.aTtl = (self.aTtl or 0) + dt

		if(myFrame.ttl == nil) then myFrame.ttl = 0 end
 
		local adjVal = tonumber(self.vars[myFrame.adj]) or 1
		
		--// If we're STILL not breaking out, check the ttl value and see if the frame has expired.
		if((nxAct == nil) and (self.curTtl > (myFrame.ttl * adjVal)) and ((myFrame.ttl * adjVal) ~= 0)) then

			--// It has, so..
			self.curTtl = nil
			nxFrm = self.frame + 1
			if(nxFrm >= (#myAction.frame + 1)) then
				if(myAction.loop ~= 'yes' and myAction.nx ~= nil) then 
					ta,tb,nxAct,tc = string.find(myAction.nx, '^(.+)%.(.+)$')
				end
				nxFrm = tonumber(tc) or 1
			end
		end
		
		-- Last thing to check: If AI is assigned, not restricted, and TTL has expired, run it.
		if((nxAct == nil) and (self.ai ~= nil)) then
			self.aiTtl = (self.aiTtl or 0) + dt
		
			if(self.aiTtl >= (gconf.aiTtl or .25) and 
				myFrame.noAI ~= 1 and 
				myAction.noAI ~= 1) 
			then
				
				self.tmpScr = AI[self.ai]
				local retStr = self:tmpScr(self.aiTtl)
				self.aiTtl = (0 - (love.math.random() * .1))
				if(type(retStr) == 'string') then
					ta,tb,nxAct,nxFrm = string.find(retStr, '^(.+)%.(.+)$')
				end
			end
		end

	end

	if (nxAct ~= nil) then 
		self.nxAct = nxAct
		self.nxFrm = nxFrm
	end
	if (nxFrm ~= nil) then 
		self.nxFrm = nxFrm or 1
	end

	--//this.move2(dt);
end

function Actor:execLogicNode(t_logic, b_isAction, o_that)

	local dt = self.timer:dt()

	if((t_logic == nil) or (type(t_logic) ~= 'table')) then return nil end

	local retAct = nil
	local retFrm = nil

	local ta,tb,tc

	--// Are there collisions?  Those come first.
	if (dt > 0 and t_logic.collision ~= nil) then

		for k,v in ipairs(t_logic.collision) do

			retAct,retFrm = self:checkCollisions(v)

			if(retAct ~= nil) then return retAct,retFrm end

		end

	end
	
	--// Is there an initscript?  Run it.
	if(t_logic.initscript ~= nil and (b_isAction == false and self.curTtl == nil)) then

		if (type(t_logic.initscript) == "string") then
			t_logic.initscript = loadstring('local self,that = ... '.. t_logic.initscript)
		end

		if (type(t_logic.initscript) == "function") then
			self.tmpScr = t_logic.initscript
			local retStr = self:tmpScr(o_that)		-- Run it on the obj.
			if(type(retStr) == 'string') then
				ta,tb,retAct,retFrm = string.find(retStr, '^(.+)%.(.+)$')
			end

		end

		if(retAct ~= nil) then return retAct,retFrm end
	
	end

	--// Are there inputs?
	if (dt > 0 and t_logic.input ~= nil) then

		for k,v in ipairs(t_logic.input) do
			local v = t_logic.input[k]

			local eval = self:checkInput(v.key, v.state, v.len)

			if (eval) then
				retAct,retFrm = self:execLogicNode(v, false, o_that)
				
				if(retAct ~= nil) then return retAct,retFrm end

				if (type(v.nx) == 'string') then
					ta,tb,retAct,retFrm = string.find(v.nx, '^(.+)%.(.+)$')
				end
				
			end

			if(retAct ~= nil) then return retAct,retFrm end

		end

	end

	--// Are there regular scripts?
	if (dt > 0 and t_logic.script ~= nil) then

		for k,v in ipairs(t_logic.script) do
		
			if (type(v) == "string") then
				t_logic.script[k] = loadstring('local self,that = ... '.. t_logic.script[k])
			end
	
			if (type(t_logic.script[k]) == 'function') then
				self.tmpScr = t_logic.script[k]
				local retStr = self:tmpScr(o_that)
				if(type(retStr) == 'string') then
					ta,tb,retAct,retFrm = string.find(retStr, '^(.+)%.(.+)$')
				end
	
			end

			if(retAct ~= nil) then return retAct,retFrm end

		end

	end

	if(retAct ~= nil) then return retAct,retFrm end
	
	return nil

end

function Actor:moveSimple(dt)
	local bnd = self:getBounds(true)
	self.lastBnd = self.lastBnd or {}
	self.lastBnd[0] = bnd[0]
	self.lastBnd[1] = bnd[1]
	self.lastBnd[2] = bnd[2]
	self.lastBnd[3] = bnd[3]

	if(dt > 0) then
		if(self.xv ~= nil) then self.x = self.x + (self.xv*dt) end
		if(self.yv ~= nil) then self.y = self.y + (self.yv*dt) end
	end
end

function Actor:spawn(template, x, y, z, isGlobalTemplate)

	local t = template

	local nx = x or 0
	local ny = y or 0
	local nz = z or 0
	
	local nobj = self.scene:spawn(t, nx, ny, nz)
	
	nobj.camera = self.camera

	return nobj
	
end

function Actor:spawnHelper(id, x, y, z)

	local t = nil
	
	if(type(id) == 'table') then
		t = id
	else
		t = self.scene.env.itemLib.items[id].helperTemplate
	end

	local h = self:spawn(t, 0, 0, self.z + .01)

	h.facing = self.facing
	h.faction = self.faction
	h.refs.parentObj = self
	h.prio = (self.prio or 100) + 1

	return h

end

function Actor:checkCollisions(criteria)
	
	local retAct = nil
	local retFrm = nil

	local ta = nil
	local tb = nil
	
	if(self.refs.collisions == nil or not(self:canCollide())) then return nil end

	for k,v in pairs(self.refs.collisions) do
		
		local that = v.obj
		
		local f = criteria.faction
		
		-- Need to make this a positive match (no 'continue' statement in Lua)
		if (
			((f == '!') and (bit.band(self.faction, that.faction) == 0)) or
			((f == '.') and (bit.band(self.faction, that.faction) ~= 0)) or
			((type(f) == 'number') and (that.faction == f))
		) then
		
			local t = criteria.objType
		
			if(t == "*" or string.match(t,that.objType)) then 

				retAct,retFrm = self:execLogicNode(criteria, false, v)

				if(retAct ~= nil) then return retAct,retFrm end

				if (type(criteria.nx) == 'string') then
					ta,tb,retAct,retFrm = string.find(criteria.nx, '^(.+)%.(.+)$')
				end

				if(retAct ~= nil) then return retAct,retFrm end

			end
		
		--retVal = this.tryLogicNode(criteria, false, that);
		
		end


		if (retAct ~= nil) then break end 

	end
	
	return retAct,retFrm
	
end

function Actor:checkInput(key, state, len)

	if((self.input == nil) or (key == nil) or (state == nil)) then return false end

	local mappedKey = key

	if(self.lrInput == nil) then self:setFacingKeys() end

	if((key == 'forward') or (key == 'back')) then
		mappedKey = self.lrInput[key]
	end
	
	local mk = self.input:st(mappedKey)
	
	local retVal = ((state == mk) or ((state == 'held') and (mk == 'press')))
	
	return retVal

end

function Actor:getBounds(recalc)

	self.bnd = self.bnd or {}

	local mybb = self.bnd

	--if(!recalc && mybb[4] && (mybb[0] != null)) return mybb;
	if(recalc ~= true and mybb[4] == true and (mybb[0] ~= nil)) then return mybb end
	
	--// Default our scaling
	self.sx = self.sx or 1
	self.sy = self.sy or 1
	self.facing = self.facing or 1

	local cell = self:getCell()

	local cx
	local cy
	local tx
	local ty

	--// First, check whether we have specific values set for bounding in the Frame
	if(cell ~= nil and cell.bnd ~= nil) then
	
		local cb = cell.bnd
	
		cx = self:getX() + (units(cb.x) * self.sx * self.facing)
		cy = self:getY() + (units(cb.y) * self.sy)
		tx = cx + (units(cb.w) * self.sx * self.facing)
		ty = cy - (units(cb.h) * self.sy)
	
	else

		local lw = self.w or 2
		local lh = self.h or 2
	
		--// BS default values - 2x2, whatever
		cx = self:getX() --(lw/2)
		cy = self:getY() - lh -- (lh/2)
		tx = cx + (lw * self.facing)
		ty = cy + lh
	
	end

	--// Use Math.min/max to order them properly.  Or something.
	local m = math
	
	mybb[0] = m.min(cx,tx)
	mybb[1] = m.min(cy,ty)
	mybb[2] = m.max(cx,tx)
	mybb[3] = m.max(cy,ty)
	mybb[4] = true
	
	return mybb

end

function Actor:getYVel()

	local yv = self.yv or 0

	if(self.refs.gndObj ~= nil) then
		yv = yv + self.refs.gndObj:getYVel()
	end

	return yv

end

function Actor:getXVel()

	local xv = self.xv or 0

	if(self.refs.gndObj ~= nil) then
		xv = xv + self.refs.gndObj:getXVel()
	end

	return xv

end


function Actor:move(dt)
	
	local xv = self:getXVel()
	local yv = self:getYVel()

	local bnd = self:getBounds(true)
	self.lastBnd = self.lastBnd or {}
	self.lastBnd[0] = bnd[0]
	self.lastBnd[1] = bnd[1]
	self.lastBnd[2] = bnd[2]
	self.lastBnd[3] = bnd[3]

	local myCam = self.camera or -1
	
	--[[// move first in x direction
	/*

		Note: This doesn't account for bounding box changing dimensions, so if it grows/shrinks, it won't be accounted for.  Meh.

	*/]]--

	--// Get our updated bounding box
	local mybb = bnd
	local x1 = mybb[0]
	local y1 = mybb[1]
	local x2 = mybb[2]
	local y2 = mybb[3]

	if(dt <= 0) then return nil end

	--// Fetch our leading edge
	local ox = 0
	local px = 0
	local stx = 1
	local checkside = 'left'
	if (xv > 0) then
		ox = x2
		stx = 1 
	elseif(xv < 0) then
		ox = x1
		stx = -1
		checkside = 'right'
	else 
		doX = false
	end

	if(self.scene == nil) then doX = false end
	
	local nx = ox + (xv * dt)

	--// Only do x-collision if we're moving to a different block.  Otherwise, just move.
	local doX = (math.floor(ox) ~= math.floor(nx))

	local xColl = false
	
	if(doX) then

		-- lx == clear x position.  kinda.
		local lx = .5 + (stx * -0.50001)
		local lcx = 0
	
		-- Check each tile our leading edge runs into
		for tcx = math.floor(ox + stx),math.floor(nx),stx do
			lcx = tcx + lx 
			for tcy = math.floor(y1),math.floor(y2) do
				local lTile = self.scene:getTile(myCam, tcx, tcy)--;//mapLib.getTile(tcx,tcy)
				if(lTile ~= nil) then
					local coll = tonumber(lTile.coll) or 0
					if(bit.band(coll, Actor.DIRBITS[checkside]) ~= 0) then xColl = true break end
				end
			end
			if (xColl == true) then break end
		end
	
		if (xColl) then
			local xDiff = lcx - ox
			local jx = self.x
			self.x = self.x + xDiff

			if(xv > 0) then
				self.wallRt = 1
				self.xv = 0.5 
			else 
				self.wallLt = 1
				self.xv = -.5 
			end
		else
			self.x = self.x + (xv * dt)
			self.wallRt = 0
			self.wallLt = 0
		end
	else

		self.x = self.x + (xv * dt)
		self.wallRt = 0
		self.wallLt = 0

	end

	--// {, do the y direction.
	--// Get our updated bounding box
	mybb = self:getBounds(true)
	x1 = mybb[0]
	y1 = mybb[1]
	x2 = mybb[2]
	y2 = mybb[3]

	--// Fetch our leading edge
	local oy = 0
	local py = 0
	local sty = 1
	if (yv > 0) then
		oy = y2
		sty = 1 
		checkside = 'up'
	elseif(yv < 0) then
		oy = y1 
		sty = -1
		checkside = 'down'
	else 
		doY = false 
	end

	if(self.scene == nil) then doY = false end
	
	local ny = oy + (yv * dt)

	--// Only do x-collision if we're moving to a different block.  Otherwise, just move.
	local doY = (math.floor(oy) ~= math.floor(ny))

	local yColl = false
	
	if(doY) then

		-- ly == clear y position.  kinda.
		local ly = .5 + (sty * -0.50001)
		local lcy = 0
	
		-- Check each tile our leading edge runs into
		for tcy = math.floor(oy + sty),math.floor(ny),sty do
			lcy = tcy + ly 
			for tcx = math.floor(x1),math.floor(x2) do
				local lTile = self.scene:getTile(myCam, tcx, tcy) --mapLib:getTile(tcx,tcy)
				if(lTile ~= nil) then
					local coll = tonumber(lTile.coll) or 0
					if(bit.band(coll, Actor.DIRBITS[checkside]) ~= 0) then yColl = true break end
				end
			end
			if (yColl == true) then break end
		end
	
		if (yColl) then
			local yDiff = lcy - oy
			local jy = self.y
			self.y = self.y + yDiff

			if(yv > 0) then
				self.grounded = 1
				self.yv = 0.5 
			else 
				self.ceiling = 1
				self.yv = 0.1 
			end
		else
			self.y = self.y + (yv * dt)
			self.grounded = 0
			self.ceiling = 0
		end
	else

		self.y = self.y + (yv * dt)
		self.grounded = 0
		self.ceiling = 0

	end
	
end

function Actor:applyForces()

	local dt = self.timer:dt()

	if(dt == 0) then return nil end

	local grav = 32
	local maxf = 40
	local fric = 30
	local airfric = 8

	if(self.scene) then
		grav = self.scene.vars.gravity or grav
		maxf = self.scene.vars.maxfall or maxf
		fric = self.scene.vars.friction or fric
		airfric = self.scene.vars.airfriction or airfric
	end

	local xv = self.xv or 0

	local xfric = 0
	
	if(self:applyAirFriction()) then
		xfric = airfric
	end

	if(self.grounded == 1 or self.grounded == true) then xfric = fric end
	
	if(xv ~= 0) then

		local stp = xfric * dt

		if (xv > 0) then
			stp = stp * -1
		end

		if (math.abs(xv) < math.abs(stp)) then
			xv = 0
		else
			xv = xv + stp
		end
		
	end

	self.xv = xv

	local yv = self.yv or 0

	yv = math.min(yv + (grav * dt), maxf)

	self.yv = yv

end

function Actor:isSolid()

	local frm = self.refs.frame or self:getFrame()

	return (self.solid == 1) or (frm ~= nil and frm.solid == 1)

end

function Actor:applyAirFriction()

	local frm = self.refs.frame or self:getFrame()

	return (self.applyAirFriction == 1) or (frm ~= nil and frm.applyAirFriction == 1)

end

function Actor:collPhysics()

	if(not(self:isSolid())) then return false end

	self.refs.collPhysArray = self.refs.collPhysArray or Queryable.new(self.refs.collisions
			):Where("x => (x.phys == nil) and x.obj:isSolid()"
			):OrderByDesc("x => x.zone[3]")
			
	local colls = self.refs.collPhysArray:ToArray()

	for k,v in ipairs(colls) do
	
		if(v.phys == nil) then
		
			local m = self
			local n = v.obj

			local mb = m.lastBnd
			local nb = n.lastBnd
			
			local wasOvX = ((mb[0] < nb[2]) and (nb[0] < mb[2]))
			local wasOvY = ((mb[1] < nb[3]) and (nb[1] < mb[3]))

			local oc = n.refs.collisions[self]
		
			local tmass = (m.mass or 1) + (n.mass or 1)
	
			local a = self:getBounds(true)
			local b = n:getBounds(true)
--			local z = {}
			local zl = 0
			local zt = 0
			local zr = 0
			local zb = 0
			zl,zt,zr,zb = m:getOverlap(n)
			local sdir = 0
			local tdir = 0
			local ydiff = math.abs(zb - zt) + .00001
			local xdiff = math.abs(zr - zl) + .00001
			local isVert = (wasOvX and not(wasOvY)) or (ydiff < xdiff)
			if(isVert) then
				if((nb[1] > mb[3]) or (a[1] < zt)) then
					v.phys = 1
					--oc.phys = nil
					n:collPhysics()
				else
					v.dir = Actor.DIRBITS.up
					oc.dir = Actor.DIRBITS.down
					v.phys = 1
					oc.phys = 1
					n = self
					m = v.obj
			
					if ((n.grounded == 1) or (n.fixed == 1)) then
						m.y = m.y - ydiff
						if((m.yv or 0) > 0.5) then 
							m.yv = 0.5 
						end
					elseif ((m.ceiling == 1) or (m.fixed == 1)) then
						n.y = n.y + ydiff
						if((n.yv or 0) < 0) then 
							n.yv = 0.1 
						end
					else
						local ymove = (ydiff * (n.mass or 1) / tmass)
						n.y = n.y + ymove
						m.y = m.y - (ydiff - ymove)
						if(m.yv > .5) then m.yv = .5 end
						if(n.yv < 0) then n.yv = .1 end
					end
					n.ceiling = 1
					m.grounded = 1
					if(n.platform == 1) then
						m.refs.gndObj = n
						n.prio = (m.prio or 0) - .1
					end
				end
			else
				if(zr < a[2]) then
					v.phys = 1
					--oc.phys = nil
					v.obj:collPhysics()
				else
					v.dir = Actor.DIRBITS.right
					oc.dir = Actor.DIRBITS.left
					v.phys = 1
					oc.phys = 1
				
					if ((m.wallLt == 1) or (m.fixed == 1)) then
						n.x = n.x + xdiff
					elseif ((n.wallRt == 1) or (n.fixed == 1)) then
						m.x = m.x - xdiff
					else
						local xmove = (xdiff * (n.mass or 1) / tmass)
						n.x = n.x + xmove
						m.x = m.x - (xdiff - xmove)
					end
					n.wallLt = 1
					m.wallRt = 1
				end
			end
		end
	end

end

function Actor:setFacingKeys()

	self.facing = self.facing or 1

	self.lrInput = self.lrInput or {}
	
	if(self.facing > 0) then
		self.lrInput.forward = 'right'
		self.lrInput.back = 'left'
	else
		self.lrInput.forward = 'left'
		self.lrInput.back = 'right'
	end

end

function Actor:broadcastEvent(ev)

	if(type(ev) ~= 'table') then return false end

	if(self.scene ~= nil) then
		return self.scene:sendEvent(ev)
	end

	return false

end

function Actor:pushEvent(ev)
	
	if(type(ev) ~= 'table') then return false end

	if(self.refs.eventQueue == nil) then
		self.refs.eventQueue = {}
		self.refs.eventQueueHead = 1
		self.refs.eventQueueTail = 1
	end

	--table.insert(self.eventQueue, ev)
	self.refs.eventQueue[self.refs.eventQueueTail] = ev
	self.refs.eventQueueTail = self.refs.eventQueueTail + 1

end

function Actor:checkSceneEvents()

	local q = self.scene.eventQueue

	local curPos = self.eventPos
	local retAct,retFrm

	for c = curPos+1,#q do
		self.eventPos = c
		local ev = q[c]
		retAct,retFrm = self:tryHandleEvent(ev)
		if(retAct ~= nil) then return retAct,retFrm end
	end

	return nil

end

function Actor:checkLocalEvents()

	local eq = self.refs.eventQueue

	local qh = self.refs.eventQueueHead
	local qt = self.refs.eventQueueTail

	for c = qh,qt-1 do
		local ev = eq[c]
		self.refs.eventQueueHead = c + 1
		eq[c] = nil

		retAct,retFrm = self:tryHandleEvent(ev)
		if(retAct ~= nil) then return retAct,retFrm end
	end

end

function Actor:tryHandleEvent(ev)

	if(type(ev) ~= 'table') then return nil end

	local frm = self:getFrame()

	if(type(frm) == 'table' and type(frm.event) == 'table') then

		for k,v in ipairs(frm.event) do

			if(type(frm.event[k].filter) == 'string') then
				frm.event[k].filter = loadstring('local self,ev = ... ' .. frm.event[k].filter)
			end

			if(frm.event[k].type == ev.type and (frm.event[k].filter ~= 'function' or frm.event[k].filter(self,ev))) then
				return self:execLogicNode(frm.event[k], false, ev)
			end

		end

	end

	local act = self:getAction()

	if(type(act) == 'table' and type(act.event) == 'table') then

		for k,v in ipairs(act.event) do

			if(type(act.event[k].filter) == 'string') then
				act.event[k].filter = loadstring('local self,ev = ... ' .. act.event[k].filter)
			end

			if(act.event[k].type == ev.type and (type(act.event[k].filter) ~= 'function' or act.event[k].filter(self,ev))) then
				return self:execLogicNode(act.event[k], false, ev)
			end

		end

	end

	local acs = self:getActionSet()

	if(type(acs) == 'table' and type(acs.event) == 'table') then

		for k,v in ipairs(acs.event) do

			if(type(acs.event[k].filter) == 'string') then
				acs.event[k].filter = loadstring('local self,ev = ... ' .. acs.event[k].filter)
			end

			if(acs.event[k].type == ev.type and (type(acs.event[k].filter) ~= 'function' or acs.event[k].filter(self,ev))) then
				return self:execLogicNode(acs.event[k], false, ev)
			end

		end

	end

	return nil

end

function Actor:checkFaction(that)

	if(type(self.faction) ~= 'number' or type(that.faction) ~= 'number') then return false end

	return (bit.band(self.faction, that.faction) ~= 0)

end

function Actor:teleport(x,y,z,sc)

	local lx = x or 0
	local ly = y or 0
	local lz = z or 0

	if(type(sc) == 'string') then
		table.insert(self.scene.env.importActors, {obj=self,x=lx,y=ly,z=lz})
		self.scene.env:requestScene(sc)
	else
		self.x = lx
		self.y = ly
		self.z = lz
	end

end

function Actor:doorTravel(door)

	local doorId = string.sub(door.tiledObjName, -2)
	
	local exit = self.scene.config.exit[doorId] --Queryable.new(self.scene.config.exit):First('x => x.id == '..doorId)
	
	----debugPrint(self.scene.config.exit, 2)
	
	if(exit ~= nil) then
		local a,b,map,toDoor = string.find(exit, '([0-9]+)%.([1248we]+)')
		table.insert(self.scene.env.importActors, {obj=self,toDoor = toDoor})
		self.scene.env:requestScene(self.scene.relPath .. '/' .. map .. '.xml')
		return map,toDoor
	end

end

function Actor:getOverlap(that)

	local o = that:getBounds(true)
	local c = self:getBounds(true)

	local m = math

	if ((c[0] <= o[2]) and (o[0] <= c[2]) and (c[1] <= o[3]) and (o[1] <= c[3])) then

		local l = m.max(o[0], c[0])
		local t = m.max(o[1], c[1])
		local r = m.min(o[2], c[2])
		local b = m.min(o[3], c[3])

		return l,t,r,b
		
	end
	
	return 0,0,0,0

end

function Actor:playAudio(id, loop)

	local audioLib = self.scene.audioLib

	audioLib:play(id, loop)

end

function Actor:canCollide()

	local myFrm = self:getFrame()

	if(type(myFrm) == 'table') then 
		return ((self.nocoll ~= 1) and (myFrm.nocoll ~= 1))
	else
		return (self.nocoll ~= 1) 
	end

end
