--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.utils')

defaultInputBindings = {
	version = 1,
	up = {'key.up', 'gamepad.dpup'},
	down = {'key.down', 'gamepad.dpdown'},
	left = {'key.left', 'gamepad.dpleft'},
	right = {'key.right', 'gamepad.dpright'},
	jump = {'key.'..LOVESPACE, 'gamepad.a'},	-- Well, ain't that awkward.
	menu = {'key.escape', 'gamepad.start'},
	inv = {'key.i', 'gamepad.back'},
	filter = {'key.f11'},
	aspect = {'key.f10'},
	act1 = {'key.s', 'gamepad.x'},
	act2 = {'key.d', 'gamepad.y'},
	act3 = {'key.f', 'gamepad.b'},
	throw = {'key.g', 'gamepad.rightshoulder'},
	block = {'key.r', 'gamepad.leftshoulder'},
	debug1 = {'key.f1'},
	debug2 = {'key.f2'},
	debug3 = {'key.f3'},
	accept = {'key.return', 'key.s', 'key.d', 'key.f', 'gamepad.x', 'gamepad.y', 'gamepad.b'},
	cancel = {'key.escape', 'key.'..LOVESPACE, 'gamepad.a'},
	interact = {'key.s', 'key.d', 'key.f', 'gamepad.x', 'gamepad.y', 'gamepad.b'},
	pickup = {'key.down', 'gamepad.dpdown'},
	any = {'key.s', 'key.d', 'key.f', 'key.escape', 'key.return', 'gamepad.x', 'gamepad.y', 'gamepad.b', 'gamepad.start', 'gamepad.back'}
}

-- Member fields template; used if no other template is given
InputLibraryDefaults = {

	key = {},

	input = {},

	luaClass = "InputLibrary",	-- These stick around.  They cool.
	class = "InputLibrary",
	name = "InputLibrary"

}

-- Table declaration for class
InputLibrary = {}

-- Metatable for class (to make InputLibrary table the default lookup for class methods)
InputLibrary_mt = {}
InputLibrary_mt.__index = InputLibrary

-- Constructor
function InputLibrary.new(template)

	-- Template ref
	local gTmp = nil

	-- Check if arg is a table; if not, use the default table declared above
	if type(template) == "table" then
		gTmp = template
	else
		gTmp = InputLibraryDefaults
	end

	-- Make copy from template if provided, or default template if not
	local gObj = tableCopy(gTmp)

	-- Set metatable to get our class methods
	setmetatable(gObj, InputLibrary_mt)

	-- Initialize with default map
--[[	for ik,iv in pairs(defaultInputBindings) do
		for kk,kv in pairs(iv) do
			gObj:addBinding(ik,kv)
		end
	end]]--
	gObj:init()

	-- Return object ref
	return gObj

end

-- init: moving from instantiated to just a global instance.  Meh.
function InputLibrary:init(filename)

	self.key = {}
	
	self.keyMap = {}

	self.input = {}

	self.luaClass = "InputLibrary"	-- These stick around.  They cool.
	self.class = "InputLibrary"
	self.name = "InputLibrary"

	self.lastPress = ""

	local bindings = defaultInputBindings
	
	if(filename ~= nil) then
		bindings = Xml.deserializeFile(filename)
		-- Create file if it doesn't exist already
		if(bindings == nil or ((tonumber(bindings.version) or 0) < defaultInputBindings.version)) then
			bindings = defaultInputBindings
			local bindStr = Xml.serialize(bindings, 'bindings', 'bindings')
			local tFile = love.filesystem.newFile(filename)
			tFile:open("w")
			tFile:write(bindStr)
			tFile:flush()
			tFile:close()
		end
	end

	-- Initialize with default map
	for ik,iv in pairs(bindings) do
		if(type(iv) == 'table') then
			for kk,kv in pairs(iv) do
				self:addBinding(ik,kv)
			end
		end
	end

	self:grabInput()

	return true

end

function InputLibrary:resetKeyStates()

	for kk,vv in pairs(self.key) do
		self:setKeyState(kk,false)
	end

end

function InputLibrary:setKeyState(key, state)

	-- if state then print('key:'..key) end

	-- Exit if invalid type
	if type(key) ~= 'string' then return nil end

	-- Okay, we're valid - set the value and jet.
	self.key[key] = state

	local km = self.keyMap[key]
	if(type(km) == 'table') then
		for k,v in ipairs(km) do
			if(state) then
				v.p = v.p + 1
				v.h = true
			else
				v.r = v.r + 1
				v.h = false
			end
		end
	end

	return true

end

function InputLibrary:addInput(input)

	self.input[input] = {
		binding = {},
		len = 0,
		p = 0,
		r = 0,
		h = false,
		st = 'open',
		ack = false
	}

end

function InputLibrary:grabInput()

	love.keypressed = 
		function(key)
			InputLibrary.lastPress = key
			InputLibrary:setKeyState('key.'..key, true)
		end

	love.keyreleased = 
		function(key)
			InputLibrary.lastPress = key
			InputLibrary:setKeyState('key.'..key, false)
		end

	love.gamepadpressed = 
		function(pad, key)
			InputLibrary.lastPress = key
			InputLibrary:setKeyState('gamepad.'..key, true)
		end

	love.gamepadreleased = 
		function(pad, key)
			InputLibrary.lastPress = key
			InputLibrary:setKeyState('gamepad.'..key, false)
		end

	love.gamepadaxis = 
		function(joystick, axis, val)
			local btn = nil
			local state = false
			if(axis == 'leftx') then
				if(math.abs(val) > .3) then
					state = true
				end
				if(val > 0) then
					btn = 'hatright'
				else
					btn = 'hatleft'
				end
			elseif(axis == 'lefty') then
				if(math.abs(val) > .3) then
					state = true
				end
				if(val > 0) then
					btn = 'hatdown'
				else
					btn = 'hatup'
				end
			end
			InputLibrary.lastPress = axis
			if(btn ~= nil) then
				InputLibrary:setKeyState('gamepad.'..btn, state)
			end

		end

end

function InputLibrary:addBinding(input, key)

	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	table.insert(self.input[input].binding, key)
	
	self.keyMap[key] = self.keyMap[key] or {}
	table.insert(self.keyMap[key], self.input[input])

end

function InputLibrary:deleteBinding(input, key)

	if type(self.input[input]) ~= 'table' or type(self.input[input].binding ~= 'table') then return false end

	for k,v in pairs(self.input[input].binding) do
		if v == key then
			table.remove(self.input[input].binding, k)
		end
	end

end

function InputLibrary:updateInputStates(dt)

	-- Loop through all defined inputs
	for ik,iv in pairs(self.input) do
		local thisState = false

		-- Loop through all bindings for this input; if any bound key exists and is active, input state is true
		for bk,bv in pairs(iv.binding) do
			if (self.key[bv] ~= nil and self.key[bv] ~= 0 and self.key[bv] ~= false) then
				thisState = true
				break
			end
		end

		local inp = self.input[ik]
		thisState = inp.h

		-- If the input is active, increment our held length by the delta value
		if thisState == true or inp.p > 0 then
			if inp.len == 0 then 
				inp.len = dt
				inp.ack = false
				inp.st = 'press'
			else
				inp.len = inp.len + dt
				inp.st = 'held'
			end
		else
			if inp.len > 0 then 
				inp.len = 0
				inp.ack = false
				inp.st = 'release'
			else
				inp.st = 'open'
			end
		end
		
		inp.p = 0
		inp.r = 0
	end

end

function InputLibrary:updateInputStates_old(dt)

	-- Loop through all defined inputs
	for ik,iv in pairs(self.input) do
		local thisState = false

		local inp = self.input[ik]

		-- If the input is active, increment our held length by the delta value
		if thisState == true then
			if inp.len == 0 then 
				inp.len = dt
				inp.ack = false
				inp.st = 'press'
			else
				inp.len = inp.len + dt
				inp.st = 'held'
			end
		else
			if inp.len > 0 then 
				inp.len = 0
				inp.ack = false
				inp.st = 'release'
			else
				inp.st = 'open'
			end
		end
	end

end

function InputLibrary:len(input)

	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	return self.input[input].len

end

function InputLibrary:st(input)
	
	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	return self.input[input].st

end

function InputLibrary:ack(input)
	
	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	return self.input[input].ack

end

function InputLibrary:setAck(input, ack)

	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	if (ack) then
		self.input[input].ack = true
	else
		self.input[input].ack = false
	end

end

-- Simple test of class members
function InputLibrary:whoAmI()

	if self.name ~= nil then print("My name is "..self.name) else print("I am nameless.") end

end

-- ToString method (needs work)
function InputLibrary:toString()

	local str = ""

	if self.name ~= nil then str = self.name
	elseif self.class ~= nil then str = self.class
	elseif self.luaClass ~= nil then str = self.luaClass
	else str = "InputLibrary" end

	return str

end
