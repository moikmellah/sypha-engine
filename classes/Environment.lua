--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.utils')
require('classes.Queryable')
require('classes.Actor')
require('classes.AudioLibrary')
require('classes.ImageLibrary')
require('classes.ItemLibrary')
require('classes.InputLibrary')
require('classes.ActionLibrary')
require('classes.ActorLibrary')
require('classes.TextLibrary')
require('classes.TilesetLibrary')
require('classes.Scene')
require('classes.DeltaTimer')
ProFi = require('classes.ProFi')

-- Table declaration for class
Environment = {}

-- Metatable for class (to make Cell table the default lookup for class methods)
Environment_mt = {}
Environment_mt.__index = Environment

-- Constructor
function Environment.new(stereo)

	-- Make copy from template if provided, or default template if not
	local newEnv = {}

	-- Set metatable to get our class methods
	setmetatable(newEnv, Environment_mt)

	newEnv.scenes = {}
	newEnv.vars = {}
	newEnv.flags = {}
	newEnv.conf = {}
	newEnv.input = InputLibrary
	newEnv.input:init()
	newEnv.itemLib = ItemLibrary
	newEnv.itemLib:init()
	newEnv.tileLib = TilesetLibrary
	newEnv.tileLib:init()
	newEnv.imgLib = ImageLibrary.new()
	newEnv.audioLib = AudioLibrary.new()
	newEnv.actionLib = ActionLibrary.new()
	newEnv.actorLib = ActorLibrary
	newEnv.actorLib:init()
	newEnv.txtLib = TextLibrary
	newEnv.actors = {}
	newEnv.importActors = {}

	newEnv.screenfx = {r=1,g=1,b=1,a=.5,hs=1,vs=1,blendMode='subtract',visible=0}
	newEnv.audiofx = 1

	newEnv.camCanvas = {}

	newEnv.debug = false
	newEnv.dtStack = {}

	newEnv.mousepos = {x=0,y=0}

	newEnv.sceneList = {}
	newEnv.sceneStack = {}

	newEnv.timers = {}
	newEnv.timers.master = DeltaTimer.new()

	newEnv.timers.scene = DeltaTimer.new(newEnv.timers.master)
	
	newEnv.timers.bkg = DeltaTimer.new(newEnv.timers.scene)
	newEnv.timers.hero = DeltaTimer.new(newEnv.timers.scene)
	newEnv.timers.boss = DeltaTimer.new(newEnv.timers.scene)
	newEnv.timers.grunt = DeltaTimer.new(newEnv.timers.scene)
	
	newEnv.sceneRequest = nil

	love.graphics.setDefaultImageFilter('linear', 'nearest')

--	newEnv.txtLib:addShader('tint', 'modules/noname/shaders/tint.txt')

	return newEnv

end

function Environment:loadConfig(fileName)

	local filePath,path = pathToFile(fileName or 'resource/conf.xml')

	self.modulePath = path
	--self.modulePath = path

	local config = Xml.deserializeFile(filePath)

	coroutine.yield(false)

	self.vars = config.vars or {}
	self.conf = config.conf or {}

	gconf = self.conf

	love.filesystem.setIdentity(gconf.moduleId)

	gconf.modulePath = self.modulePath

	self.cnv = love.graphics.newCanvas(self.conf.vpWidth, self.conf.vpHeight)
	self.cnvQuad = love.graphics.newQuad(0,0,self.conf.vpWidth,self.conf.vpHeight,self.conf.vpWidth,self.conf.vpHeight)

	if(self.conf.stereo == true) then
		self.cnv2 = love.graphics.newCanvas(self.conf.vpWidth, self.conf.vpHeight)
		self.vars.stereo = true
		self.vars.stereoMag = 1
		if(stereo < 0) then self.vars.reverseStereo = true end
	end



	-- Load extensions first.
	if(type(config.extension) == 'string') then
		local et = {}
		table.insert(et, config.extension)
		config.extension = et
	end

	if(type(config.extension) == 'table') then
		for k,v in ipairs(config.extension) do
			local ePath = string.gsub(pathToFile(self.modulePath .. '/' .. v), '%.lua', '')
			ePath = string.gsub(ePath, '/', '.')
			require(ePath)
			coroutine.yield(false)
		end
	end

	-- Load up the Actor templates
	if(type(config.actorSet) == 'table') then
		if(type(config.actorSet[1]) == 'table') then
			for k,v in ipairs(config.actorSet) do
				self.actorLib:loadSet(self.modulePath .. '/' .. v.file)
				coroutine.yield(false)
			end
		else
			self.actorLib:loadSet(self.modulePath .. '/' .. config.actorSet.file)
		end
	end

	coroutine.yield(false)

	-- Now the ActionSets
	if(type(config.actionSet) == 'table') then
		if(type(config.actionSet[1]) == 'table') then
			for k,v in ipairs(config.actionSet) do
				self.actionLib:loadSet(self.modulePath .. '/' .. v.file)
				coroutine.yield(false)
			end
		else
			self.actionLib:loadSet(self.modulePath .. '/' .. config.actionSet.file)
		end
	end

	coroutine.yield(false)

	-- Now the ItemSets
	if(type(config.itemSet) == 'table') then
		if(type(config.itemSet[1]) == 'table') then
			for k,v in ipairs(config.itemSet) do
				self.itemLib:loadSet(self.modulePath .. '/' .. v.file)
				coroutine.yield(false)
			end
		else
			self.itemLib:loadSet(self.modulePath .. '/' .. config.itemSet.file)
		end
	end
	
	coroutine.yield(false)
	
	-- ImageSets
	if(type(config.imageSet) == 'table') then
		for k,v in pairs(config.imageSet) do
			if(type(v) == 'table') then 
				if(v.imgFile ~= nil) then
					self.imgLib:loadSet(k, self.modulePath .. '/' .. v.atlasFile, self.modulePath .. '/' .. v.imgFile)
				else
					self.imgLib:loadSet(k, self.modulePath .. '/' .. v.atlasFile)
				end
			end
			coroutine.yield(false)
		end
	end

	-- Audio
	if(type(config.audio) == 'table') then
		for k,v in pairs(config.audio) do
			if(type(v) == 'table') then 
				if(v.type == 'sfx') then
					self.audioLib:loadAudioFile(k, self.modulePath .. '/' .. v.path, 10)
				else
					self.audioLib:loadAudioFile(k, self.modulePath .. '/' .. v.path, 1)
				end
			end
			coroutine.yield(false)
		end
	end

	-- font
	if(type(config.font) == 'table') then
		for k,v in pairs(config.font) do
			if(type(v) == 'table') then 
				if(v.imgFile ~= nil) then
					self.txtLib:loadFont(k, self.modulePath .. '/' .. v.imgFile)
				end
			end
			coroutine.yield(false)
		end
	end

	-- shaders
	if(type(config.shader) == 'table') then
		for k,v in pairs(config.shader) do
			if(type(v) == 'table') then 
				if(v.path ~= nil) then
					self.txtLib:addShader(k, self.modulePath .. '/' .. v.path)
				end
			end
			coroutine.yield(false)
		end
	end

	-- Lastly, fire off the initscript, if it exists.
	local initScript = config.initscript
	if(type(config.initscript) == 'string') then
		config.initscript = loadstring('local self,config = ... '.. config.initscript)
	end

	if(type(config.initscript) == 'function') then
		self.tmpScr = config.initscript
		self:tmpScr(config)
	end

	if(gconf.gcSetPause) then collectgarbage("setpause", gconf.gcSetPause) end
	if(gconf.gcSetStepMul) then collectgarbage("setstepmul", gconf.gcSetStepMul) end

	coroutine.yield(false)

	self:launchScene(pathToFile(self.modulePath .. '/' .. config.initScene))

	coroutine.yield(false)

end

function Environment:update(dtp)

	local dt = dtp

	if(dt > .05) then dt = .05 end

	self.timers.master:add(dt)

	if(self.sceneRequest ~= nil) then
	
		if(self.sceneRequest == 'pop') then
			self:popScene()
		else
			self:launchScene(self.sceneRequest, self.scenePush)
		end
		
		self.sceneRequest = nil
		self.scenePush = nil
		
	end

	self.audioLib.clock = (self.audioLib.clock or 0) + dt
	self.input:updateInputStates(dt)
	self.activeScene:update(dt)
	
end

function Environment:requestScene(sceneConfig, push)
	self.sceneRequest = sceneConfig
	self.scenePush = push
end

function Environment:draw()
	local c2 = nil
	love.graphics.push()
	love.graphics.setColor(0,0,0,0)
	love.graphics.setCanvas(self.cnv)
	love.graphics.clear()
	love.graphics.setCanvas()
	love.graphics.setColor(255,255,255,255)
	if(self.vars.stereo == true) then c2 = self.cnv2 end
	self.activeScene:draw(self.cnv, c2)

	if(self.screenfx.visible == 1) then
		love.graphics.setColor(
			(self.screenfx.r or 1)*255,
			(self.screenfx.g or 1)*255,
			(self.screenfx.b or 1)*255,
			math.floor((self.screenfx.a or 1)*255)
		)
		love.graphics.setBlendMode(self.screenfx.blendMode or 'alpha')
		love.graphics.setCanvas(self.cnv)
		love.graphics.rectangle('fill', 0, 0, self.conf.vpWidth, self.conf.vpHeight)
		love.graphics.setCanvas()
		love.graphics.setColor(255,255,255,255)
		love.graphics.setBlendMode('alpha')

	end

	self.audiofx = self.audiofx or 1
	gconf.audioVolume = gconf.audioVolume or 100
	self.audioLib:setMaster(self.audiofx * (gconf.audioVolume/100))

	love.graphics.pop()
end

function Environment:launchScene(sceneConfig, push)

	local sc = sceneConfig

	if(push == true and self.activeScene ~= nil) then
		table.insert(self.sceneStack, self.activeScene)
	elseif(push ~= true and self.activeScene ~= nil) then
		self.activeScene:unloadObjs()
	end
	
	if(self.sceneList[sc] == nil) then

		local scPath = pathToFile(sc)
	
		self.sceneList[sc] = Scene.new(scPath, self)
		self.sceneList[sc].input = self.input
		self.sceneList[sc].imgLib = self.imgLib
		self.sceneList[sc].audioLib = self.audioLib
		self.sceneList[sc].actionLib = self.actionLib
		self.sceneList[sc].actorLib = self.actorLib
		self.sceneList[sc].txtLib = self.txtLib
		self.sceneList[sc].tileLib = self.tileLib
		
	end
	
	self.activeScene = self.sceneList[sc]
	
	self.activeScene:init()
	self.imgLib:setFilters('linear', 'nearest')
	--self.cnv:setFilter('linear', 'nearest')
	self:setFiltering(gconf.filtering == 'ON')
end

function Environment:requestPopScene()
	self.sceneRequest = 'pop'
end

function Environment:popScene()

	if(#self.sceneStack > 0) then
		self.activeScene:unloadObjs()
		self.activeScene = table.remove(self.sceneStack)
		for k,v in pairs(self.activeScene.camera) do
			v.dirty = true
		end
	end

end

function Environment:clickHandle(x,y)
	self.mousepos.x = x
	self.mousepos.y = y
	self.activeScene:clickHandle(units(x),units(y))
end

function Environment:toggleFiltering()
--[[	if(self.vars.filtering == true) then
		self.vars.filtering = false
	else
		self.vars.filtering = true
	end

	self:setFiltering()
]]--
	if (self.vars.proFiEnabled == true) then
		ProFi:stop()
		ProFi:writeReport()
		ProFi:reset()
		self.vars.proFiEnabled = false
	else
		ProFi:start()
		self.vars.proFiEnabled = true
	end

end

function Environment:setFiltering(value)
	if (value) then
		self.cnv:setFilter('linear', 'linear', 0)
	else
		self.cnv:setFilter('nearest', 'nearest', .25)
	end
end

function Environment:setScreenFx(r,g,b,a,shader)

	self.vars.color = self.vars.color or {}
	
	if(r ~= nil) then self.vars.color.r = r end
	if(g ~= nil) then self.vars.color.g = g end
	if(b ~= nil) then self.vars.color.b = b end
	if(a ~= nil) then self.vars.color.a = a end
	if(shader ~= nil) then self.vars.color.shader = shader end
	
end

function Environment:resetScreenFx()

	self.vars.color = {}

end

function Environment:loadingScreen(s,sx,sy)
	local x = love.graphics.getWidth()/2
	local y = love.graphics.getHeight()/2

	local f = love.graphics.getFont()
	
	local w = f:getWidth(s)/2
	local h = f:getHeight()/2
	
	love.graphics.setColor(255,255,255,255)

	love.graphics.setCanvas()

	love.graphics.clear()

	self.vars.loadingR = (self.vars.loadingR or 0)

	love.graphics.printf(s, x, y, w*2, 'center', self.vars.loadingR, (x*2)/sx,(y*2)/sy, w,h)

end

function Environment:loadPrefs(filename)
	
	local fn = filename or '/userConfig.xml'
	local prefArray = Xml.deserializeFile(fn) or {}
	prefArray.frameCt = nil
	prefArray.moduleId = nil
	prefArray.moduleTitle = nil
	prefArray.modulePath = nil
	self.conf = tableMerge(self.conf, prefArray, true)

	Scene:setFullscreen(self.conf.fullscreen == 'ON')
	self:setFiltering(self.conf.filtering == 'ON')
	
end

function Environment:savePrefs(filename)

	local cnf = tableCopy(self.conf)
	
	cnf.frameCt = nil

	local prefStr = Xml.serialize(cnf, 'conf', 'conf')
	
	local tFile = love.filesystem.newFile(filename or '/userConfig.xml')
	tFile:open("w")
	tFile:write(prefStr)
	tFile:flush()
	tFile:close()

end
