--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('classes.ImageLibrary')

TextLibrary = {}

TextLibrary_mt = {}
TextLibrary_mt.__index = TextLibrary

function TextLibrary:loadFont(handle, srcFile, defStr)

	self.fonts = self.fonts or {}
	local df = defStr or ' !"#$%&' .. "'" .. '()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
	self.fonts[handle] = love.graphics.newImageFont(srcFile, df, 1)

end

function TextLibrary:setFont(handle)

	self.fonts = self.fonts or {}
	local font = self.fonts[handle]

	if(self.fonts[handle] == nil) then print('Fontnope:' .. handle) end


	if(font ~= nil) then
		love.graphics.setFont(font)
		return font
	end

	return nil

end

function TextLibrary:renderStr(ctx, str, x, y, w, h, align, font, color)
	
	if(str == nil) then return false end
	local rx = x or 0
	local ry = y or 0
	local rw = w or 0
	local rh = 1
	local ra = align or 'left'
	local f = font or 'default'
	local vl = 1
	local gfx = color

	love.graphics.push()
	love.graphics.setCanvas(ctx)
	local lgf = self:setFont(f)

	if(rw ~= 0) then
		rw,vl = lgf:getWrap(str, rw)
		if(type(vl) == 'table') then vl = #vl end
	else
		rw = lgf:getWidth(str)
	end

	if(ra == 'center') then
		rx = rx - (rw/2)
	elseif(ra == 'right') then
		rx = rx - rw
	end

	if(ctx ~= nil) then love.graphics.setCanvas(ctx) end

	if(type(gfx) == 'table') then
		love.graphics.setColor(gfx.r*255, gfx.g*255,gfx.b*255,gfx.a*255)
		self:setShader(gfx.shader)
	end

	love.graphics.printf(str, rx, ry, rw, ra)

	love.graphics.setColor(255,255,255,255)
	self:setShader()
	love.graphics.pop()

end

function TextLibrary:renderLabel(ctx, ctx2, lb, x,y,z, nsx,nsy)

	local lsx = nsx or 1
	local lsy = nsy or 1

--	if(lsx ~= 1) then --debugPrint('lsx:' .. lsx) end

	if type(lb) == "table" and lb.visible == 1 then

		love.graphics.push()
		love.graphics.setCanvas(ctx)
		local lgf = self:setFont(lb.font or 'default')

		-- Initialize critical values
		lb.x = lb.x or 0 
		lb.y = lb.y or 0 
		lb.type = lb.type or 'rect'
		lb.text = lb.text or ''
		--lb.h = lb.h or 16 
		lb.bkgPadding = lb.bkgPadding or 0 

		local ll = x + (lb.x * lsx)
		local lt = y + (lb.y * lsy)
		local bs = 0
		local vl = 1
		local sw = lb.w
		local sh = lb.h
		local ox = 0
		local oy = 0

		if(lb.type == 'label') then
			if(type(sw) ~= 'number' or sw <= 0) then
				sw = lgf:getWidth(lb.text)
			else
				bs,vl = lgf:getWrap(lb.text, sw)
				if(type(vl) == 'table') then vl = #vl end
			end
		elseif(lb.type == 'rect') then
			sw = sw or 1
			sh = sh or 1
		end

		if(type(sh) ~= 'number' or sh <= 0) then
			sh = vl * lgf:getHeight()
		end

		local lsw = sw

		sw = math.abs(sw * lsx)
		sh = math.abs(sh * lsy)

		local lb_ll = ll
		local lb_lt = lt

		-- Calculate initial bound rect values
		if(lb.align == 'center') then 
			ll = ll - (sw/2)
			ox = math.floor(sw/2)
		elseif(lb.align == 'right') then
			ll = ll - sw
			ox = math.floor(sw)
		end
		if(lb.valign == 'center') then 
			lt = lt - (sh/2)
			oy = sh/2
		elseif(lb.valign == 'bottom') then
			lt = lt - sh
			oy = sh
		end
		
		-- Rect vars
		local tl = math.min(ll, ll + sw)
		local tt = math.min(lt, lt + sh)
		local pl = tl - lb.bkgPadding
		local pt = tt - lb.bkgPadding
		local pr = sw + (lb.bkgPadding * 2)
		local pb = sh + (lb.bkgPadding * 2)
		local pad = 0

		-- Display the bkg, if present
		if type(lb.bkgColor) == "table" then
			
			love.graphics.setColor(
				(lb.bkgColor.r or 1) * 255, 
				(lb.bkgColor.g or 1) * 255, 
				(lb.bkgColor.b or 1) * 255, 
				(lb.bkgColor.a or 1) * 255
			)
			love.graphics.rectangle('fill', pl,pt, pr,pb)
		end

		if type(lb.borderWidth) == "number" then

			lb.borderColor = lb.borderColor or {}

			local linestyle = lb.borderColor.style or 'rough'

			love.graphics.setColor(
				(lb.borderColor.r or 1) * 255, 
				(lb.borderColor.g or 1) * 255, 
				(lb.borderColor.b or 1) * 255, 
				(lb.borderColor.a or 1) * 255
			)
			love.graphics.setLineWidth(lb.borderWidth)
			love.graphics.setLineStyle(linestyle)
			love.graphics.rectangle('line', pl-(lb.borderWidth/2),pt-(lb.borderWidth/2), pr+lb.borderWidth,pb+lb.borderWidth)
		
		end

		if(lb.type == 'label') then
			-- Display the string
			lb.color = lb.color or {}
			lb.yoffset = lb.yoffset or 0
			lb.xoffset = lb.xoffset or 0

			love.graphics.setColor((lb.color.r or 1) * 255, (lb.color.g or 1) * 255, 
				(lb.color.b or 1) * 255, (lb.color.a or 1) * 255)
			--love.graphics.setScissor(tl, tt, sw, sh)
			love.graphics.printf(lb.text, math.floor(lb_ll+lb.xoffset), math.floor(lb_lt + lb.yoffset), sw, lb.align or 'left', nil, lsx, lsy, ox, oy)
			love.graphics.setScissor()
		end
		love.graphics.pop()
		
		love.graphics.setColor(255,255,255,255)

	end

end

function TextLibrary:renderEllipse(ctx, ctx2, lb, x,y,z, nsx,nsy, facing)

	local lsx = nsx or 1
	local lsy = nsy or 1

	local fc = facing or 1

	if type(lb) == "table" and lb.visible == 1 then

		love.graphics.push()
		love.graphics.setCanvas(ctx)
		-- Initialize critical values
		lb.x = lb.x or 0 
		lb.y = lb.y or 0 
		--lb.bkgColor = lb.bkgColor or {}
		lb.bkgPadding = lb.bkgPadding or 0 

		local ll = x + (lb.x * lsx * facing)
		local lt = y + (lb.y * lsy)
		local bs = 0
		local vl = 1
		local sw = lb.w
		local sh = lb.h

		sw = sw or 1
		sh = sh or 1

		-- Calculate initial bound rect values
		if(lb.align == 'left') then 
			ll = ll - (sw/2*lsx)
		elseif(lb.align == 'right') then
			ll = ll + (sw/2*lsx)
		end
		if(lb.valign == 'top') then 
			lt = lt - (sh/2*lsy)
		elseif(lb.valign == 'bottom') then
			lt = lt + (sh/2*lsy)
		end
		
		-- Display the bkg, if present
		if type(lb.bkgColor) == "table" then
			
			love.graphics.setColor(
				(lb.bkgColor.r or 1) * 255, 
				(lb.bkgColor.g or 1) * 255, 
				(lb.bkgColor.b or 1) * 255, 
				(lb.bkgColor.a or 1) * 255
			)
			love.graphics.ellipse('fill', ll, lt, sw/2*math.abs(lsx), sh/2*math.abs(lsy))
		end

		if type(lb.borderWidth) == "number" then

			lb.borderColor = lb.borderColor or {}

			local linestyle = lb.borderColor.style or 'rough'

			love.graphics.setColor(
				(lb.borderColor.r or 1) * 255, 
				(lb.borderColor.g or 1) * 255, 
				(lb.borderColor.b or 1) * 255, 
				(lb.borderColor.a or 1) * 255
			)
			love.graphics.setLineWidth(lb.borderWidth)
			love.graphics.setLineStyle(linestyle)
			--love.graphics.rectangle('line', pl-(lb.borderWidth/2),pt-(lb.borderWidth/2), pr+lb.borderWidth,pb+lb.borderWidth)
			love.graphics.ellipse('line', ll, lt, sw/2*math.abs(lsx), sh/2*math.abs(lsy))
		end

		love.graphics.pop()
		
		love.graphics.setColor(255,255,255,255)

	end

end

function TextLibrary:renderLine(ctx, ctx2, lb, x,y,z, nsx,nsy)

	local lsx = nsx or 1
	local lsy = nsy or 1

	if type(lb) == "table" and lb.visible == 1 then

		love.graphics.push()
		love.graphics.setCanvas(ctx)
		-- Initialize critical values
		lb.x = lb.x or 0 
		lb.y = lb.y or 0 
		lb.bkgColor = lb.bkgColor or {}
		lb.bkgPadding = lb.bkgPadding or 0 

		local ll = x + (lb.x * lsx)
		local lt = y + (lb.y * lsy)
		local bs = 0
		local vl = 1

		lb.color = lb.color or {}

		local linestyle = lb.color.style or 'rough'

		love.graphics.setColor(
			(lb.color.r or 1) * 255, 
			(lb.color.g or 1) * 255, 
			(lb.color.b or 1) * 255, 
			(lb.color.a or 1) * 255
		)
		love.graphics.setLineWidth(lb.lineWidth or 1)
		love.graphics.setLineStyle(linestyle)

		if(type(lb.point) == 'table' and #lb.point > 2) then
			for k = 2,#lb.point do
				local sx = lb.point[k-1].x or 0
				local sy = lb.point[k-1].y or 0 
				local ex = lb.point[k].x or 0
				local ey = lb.point[k].y or 0 

				love.graphics.line(ll + (sx * lsx), lt + (sy * lsy), ll + (ex * lsx), lt + (ey * lsy))
			end
			if(lb.closed == 1) then
				local sx = lb.point[#lb.point].x or 0
				local sy = lb.point[#lb.point].y or 0 
				local ex = lb.point[1].x or 0
				local ey = lb.point[1].y or 0 

				love.graphics.line(ll + (sx * lsx), lt + (sy * lsy), ll + (ex * lsx), lt + (ey * lsy))
			end
		end

		love.graphics.pop()
		
		love.graphics.setColor(255,255,255,255)

	end

end

function TextLibrary:renderPoints(ctx, ctx2, lb, x,y,z, nsx,nsy)

	local lsx = nsx or 1
	local lsy = nsy or 1

	if type(lb) == "table" and lb.visible == 1 then

		love.graphics.push()
		love.graphics.setCanvas(ctx)
		-- Initialize critical values
		lb.x = lb.x or 0 
		lb.y = lb.y or 0 
		lb.bkgColor = lb.bkgColor or {}
		lb.bkgPadding = lb.bkgPadding or 0 

		local ll = x + (lb.x * lsx)
		local lt = y + (lb.y * lsy)
		local bs = 0
		local vl = 1

		lb.color = lb.color or {}

		love.graphics.setColor(
			(lb.color.r or 1) * 255, 
			(lb.color.g or 1) * 255, 
			(lb.color.b or 1) * 255, 
			(lb.color.a or 1) * 255
		)

		if(type(lb.point) == 'table') then
			for k = 1,#lb.point do
				local ex = lb.point[k].x or 0
				local ey = lb.point[k].y or 0 

				love.graphics.points(math.floor(ll + (ex * lsx)) + .5, math.floor(lt + (ey * lsy)) + .5)
			end
		end

		love.graphics.pop()
		
		love.graphics.setColor(255,255,255,255)

	end

end

function TextLibrary:renderImg(ctx, ctx2, lb, cl, x,y,z, nsx,nsy)

	local cell = cl or lb.cell
	
	if(cell == nil) then
		cell = {}
		cell.img = lb.img
		cell.gquad = lb.q or love.graphics.newQuad(0,0,lb.img:getWidth(),lb.img:getHeight(),lb.img:getWidth(),lb.img:getHeight())
		cell.origin = {x=0,y=0}
		lb.cell = cell
	end

	if (type(lb) == "table" and lb.visible == 1) then

		love.graphics.push()
		love.graphics.setCanvas(ctx)
		local sfc = lb.color
	
		local ll = x + (lb.x * (nsx or 1))
		local lt = y + (lb.y * (nsy or 1))
		
		if(sfc ~= nil) then
			love.graphics.setColor((sfc.r or 1)*255,(sfc.g or 1)*255,(sfc.b or 1)*255,(sfc.a or 1)*255)
			self:setShader(sfc.shader)
		end

		love.graphics.setCanvas(ctx)

		love.graphics.drawq(cell.img, cell.gquad, ll, lt, 0, nsx or 1, nsy or 1, cell.origin.x or 0, cell.origin.y or 0)
	
		if(ctx2 ~= nil) then
		
			love.graphics.setCanvas(ctx2)

			love.graphics.drawq(cell.img, cell.gquad, ll, lt, 0, nsx or 1, nsy or 1, cell.origin.x, cell.origin.y)
	
		end

		self:setShader()

		love.graphics.setColor(255,255,255,255)

		love.graphics.pop()

	end

end

function TextLibrary:addShader(s_tag, s_shader)

	self.shaders = self.shaders or {}

	self.shaders[s_tag] = self.shaders[s_tag] or love.graphics.newShader(s_shader)

end

function TextLibrary:setShader(s_tag)

	self.shaders = self.shaders or {}

	if(s_tag == nil) then
		love.graphics.setShader()
	elseif(self.shaders[s_tag] ~= nil) then
		love.graphics.setShader(self.shaders[s_tag])
	end

end
