--[[

Copyright 2016, moikmellah

Queryable.lua is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

Queryable = {}
Queryable_mt = {}

--[[

	Constructor - accepts any table as an arg.  'tailp' should only be used internally.

]]--
function Queryable.new(t, tailp)

	local qbl = {}

	qbl._contents = t or {}

	qbl._tailPredicate = tailp

	setmetatable(qbl, Queryable_mt)

	return qbl

end

--[[

	Length operator - by default, return len of underlying collection.

]]--
function Queryable_mt.__len(t)

	return #t._contents

end

--[[

	Indexer

]]--
function Queryable_mt.__index(t, k)

	if(t == nil or k == nil) then return nil end

	if(type(Queryable_mt[k]) == 'function') then return Queryable_mt[k] end

	t._contents = t._contents or {}
	return t._contents[k]

end

--[[

	Newindex - don't allow 

]]--
function Queryable_mt.__newindex(t, k, v)

	error("Queryable is read-only.")
	
end

--[[

	__parseDelta - parses a string of the form 'obj => exp', similar to .NET delta functions;
			returns a table of form {Type, Name, Clause}

]]--
function Queryable_mt.__parseDelta(ot, p, typ)

	local t = ot
	
	local myType = typ or 'where'

	if(t == 'nil' or type(p) ~= 'string') then return t end
	
	local s,e,n,c = string.find(p, '([%w]+) => (.+)')
	
	if(s == nil) then error('Invalid ' .. myType .. ' clause: (' .. p .. ')') end
	
	local np = {Type = myType, Name = n, Clause = ' '..c..' '}

	return np
	
end

--[[

	__addPredicate - Adds a predicate object; returns a new Queryable with the full predicate chain

]]--
function Queryable_mt.__addPredicate(ot, p)

	if(ot == nil or p == nil) then return nil end

	p.parent = ot._tailPredicate

	local t = Queryable.new(ot._contents, p)
	
	return t
	
end

--[[

	Where - adds a Where filter, without enumerating the series.  Requires a single delta expression as an arg.

]]--
function Queryable_mt.Where(t, p)

	return t:__addPredicate(t:__parseDelta(p, 'where'))

end

--[[

	Select - adds a Select transformation, without enumerating the series.  Requires a single delta expression as
		an arg.

]]--
function Queryable_mt.Select(t, p)

	return t:__addPredicate(t:__parseDelta(p, 'select'))

end

--[[

	OrderBy - adds a sort, without enumerating the series.  Requires a single delta expression as an
		arg; delta should return a value suitable for comparison in sort operations.

]]--
function Queryable_mt.OrderBy(t, p)

	return t:__addPredicate(t:__parseDelta(p, 'orderby'))

end

--[[

	OrderByDesc - adds a descending sort, without enumerating the series.  Requires a single delta expression as an
		arg; delta should return a value suitable for comparison in sort operations.

]]--
function Queryable_mt.OrderByDesc(t, p)

	return t:__addPredicate(t:__parseDelta(p, 'orderbydesc'))

end

--[[

	Take - adds a limit to the series, returning only the first N items, without enumerating the series.  Requires a
		single integer as an arg.

]]--
function Queryable_mt.Take(ot, p)

	return ot:__addPredicate({Type = 'take', Count = p})
	
end

--[[

	Skip - discards the first N items in a series, without enumerating the series.  Requires a single integer as an arg.

]]--
function Queryable_mt.Skip(ot, p)

	return ot:__addPredicate({Type = 'skip', Count = p})
	
end

--[[

	First - enumerates the series, and returns the first item in the series to match the provided delta expression
		(or nil if no items match).  If no delta expression is provided, simply returns the first item in the
		series.

]]--
function Queryable_mt.First(ot, pred)

	local a = ot:ToArray()

	if(type(pred) == 'string') then	
		local p = ot:__parseDelta(pred, 'where')
	
		local b = true

		local pos = 0
		local wFct = loadstring('local ' .. p.Name .. ' = ... return (' .. p.Clause .. ')')
		for ik = 1,#a do
			if(wFct(a[ik])) then
				return a[ik]
			end
		end
		return nil
	else
		return a[1]
	end

end

--[[

	Last - enumerates the series, and returns the last item in the series to match the provided delta expression
		(or nil if no items match).  If no delta expression is provided, simply returns the last item in the
		series.

]]--
function Queryable_mt.Last(ot, pred)

	local a = ot:ToArray()
	
	local p = ot:__parseDelta(pred, 'where')
	
	local b = true

	local pos = 0
	local wFct = loadstring('local ' .. p.Name .. ' = ... return (' .. p.Clause .. ')')
	for ik = #a,1,-1 do
		if(wFct(a[ik])) then
			return a[ik]
		end
	end

	return nil

end

--[[

	Any - enumerates the series, and returns true if any item matches the provided delta expression (required), or
		false if no items match.

]]--
function Queryable_mt.Any(ot, pred)

	local a = ot:ToArray()
	
	local p = ot:__parseDelta(pred, 'where')
	
	local b = true

	local pos = 0
	local wFct = loadstring('local ' .. p.Name .. ' = ... return (' .. p.Clause .. ')')
	for ik = 1,#a do
		if(wFct(a[ik])) then
			return true
		end
	end

	return false

end

--[[

	All - enumerates the series, and returns false if any item fails to match the provided delta expression
		(required), or true if all items match.

]]--
function Queryable_mt.All(ot, pred)

	local a = ot:ToArray()
	
	local p = ot:__parseDelta(pred, 'where')
	
	local b = true

	local pos = 0
	local wFct = loadstring('local ' .. p.Name .. ' = ... return (' .. p.Clause .. ')')
	for ik = 1,#a do
		if(not(wFct(a[ik]))) then
			return false
		end
	end

	return true

end

--[[

	Count - enumerates the series, and returns the count of the resulting array.  If an optional delta expression
		is provided, returns the number of items which match it.

]]--
function Queryable_mt.Count(ot, pred)

	if(pred == nil) then
		return #(ot:ToArray())
	end

	local a = ot:__addPredicate(ot:__parseDelta(pred, 'where')):ToArray()
	
	return #a

end

--[[

	ToArray - enumerates the series, and returns an indexed array with the results.  Note that neither the 
		keys nor the order of the original collection are preserved.

]]--
function Queryable_mt.ToArray(t)

	local a = {}
	
	for ak,av in pairs(t._contents) do
		a[#a+1] = av
	end

	local predicates = {}
	
	local pp = t._tailPredicate
	
	while(pp ~= nil) do
		table.insert(predicates, pp)
		pp = pp.parent
	end

	for k = #predicates,1,-1 do
		local p = predicates[k]
		if(p.Type == 'where') then
			local pos = 0
			local wFct = loadstring('local ' .. p.Name .. ' = ... return (' .. p.Clause .. ')')
			for ik = 1,#a do
				if(not(wFct(a[ik]))) then
					a[ik] = nil
					pos = pos - 1
				else
					if(pos < 0) then
						a[ik+pos] = a[ik]
						a[ik] = nil
					end
				end
			end
		elseif(p.Type == 'select') then
			local wFct = loadstring('local ' .. p.Name .. ' = ... return (' .. p.Clause .. ')')
			for ik = 1,#a do
				a[ik] = wFct(a[ik])
			end
		elseif(p.Type == 'orderby') then
			local wFct = loadstring('local _argA,_argB = ... local '..p.Name..' = _argA local _valA = ('..p.Clause..') '..p.Name..' = _argB local _valB = ('..p.Clause..') return (_valA < _valB)')
			table.sort(a, wFct)
		elseif(p.Type == 'orderbydesc') then
			local wFct = loadstring('local _argA,_argB = ... local '..p.Name..' = _argA local _valA = ('..p.Clause..') '..p.Name..' = _argB local _valB = ('..p.Clause..') return (_valB < _valA)')
			table.sort(a, wFct)
		elseif(p.Type == 'take') then
			local pos = #a-p.Count
			if(pos > 0) then
				for i = p.Count+1,#a do
					a[i] = nil
				end
			end
		elseif(p.Type == 'skip') then
			local pos = #a-p.Count
			if(pos > 0) then
				for i = p.Count+1,#a do
					a[i-p.Count] = a[i]
					a[i] = nil
				end
			else
				a = {}
			end
		end
	end

	return a

end

--[[

	ToLookup - enumerates the series, and returns an associative array with the results.  Takes a required key
		selector delta expression, and an optional transformational delta expression. 

]]--
function Queryable_mt.ToLookup(ot, predK, predV)

	if(type(predK) ~= "string") then error("Key selector not a valid delta.") end

	local pk = ot:__parseDelta(predK, 'where')
	
	local vFct = nil
	
	if(type(predV) == "string") then
		local pv = ot:__parseDelta(predV, 'where')
		vFct = loadstring('local ' .. pv.Name .. ' = ... return (' .. pv.Clause .. ')')
	end

	local a = ot:ToArray()
	
	local d = {}

	local kFct = loadstring('local ' .. pk.Name .. ' = ... return (' .. pk.Clause .. ')')

	for i = 1,#a do
	
		local dk = kFct(a[i])
		if(type(vFct) == 'function') then
			dv = vFct(a[i])
		else
			dv = a[i]
		end
		
		d[dk] = dv
	
	end
	
	return d

end

--[[

	GroupBy - enumerates the series, and returns a two-dimensional array of the results, grouped by a given value.
		Takes a single required delta expression for key selection.

]]--
function Queryable_mt.GroupBy(ot, pred)

	if(type(pred) ~= "string") then error("Key selector not a valid delta.") end

	local pk = ot:__parseDelta(pred, 'where')
	
	local a = ot:ToArray()
	
	local d = {}

	local kFct = loadstring('local ' .. pk.Name .. ' = ... return (' .. pk.Clause .. ')')

	for i = 1,#a do
	
		local dk = kFct(a[i])
		
		d[dk] = d[dk] or {}
		
		table.insert(d[dk], a[i])
	
	end
	
	return d

end
