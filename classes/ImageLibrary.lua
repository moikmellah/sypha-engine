--[[

Copyright 2016, Project SYPHA 

The SYPHA Engine is released under the MIT license.  A copy of this license should be
distributed with this software, and may otherwise be viewed at http://mit-license.org/

]]--

require('utils.xml')

-- Table declaration for class
ImageLibrary = {}

-- Metatable for class (to make ImageLibrary table the default lookup for class methods)
ImageLibrary_mt = {}
ImageLibrary_mt.__index = ImageLibrary

-- Constructor
function ImageLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nImgLib = {}

	-- Set metatable to get our class methods
	setmetatable(nImgLib, ImageLibrary_mt)

	nImgLib.images = {}
	nImgLib.count = 0

	nImgLib.spbs = {}

	return nImgLib

end

function ImageLibrary:setFilters(filter1, filter2)

	for k,v in pairs(self.imageSrc) do
		if v ~= nil and type(v) == 'userdata' then v:setFilter(filter1, filter2) end
	end

--[[	for k,v in pairs(self.images) do
		if v ~= nil and type(v) == 'userdata' then v:setFilter(filter1, filter2)
		elseif v ~= nil and type(v) == 'table' then v.img:setFilter(filter1, filter2)
		end
	end
]]--

end

function ImageLibrary:loadImageFile(handle,filePath)

	if (handle == nil) then return false end

	if self.images == nil then self.images = {} end

	self.count = self.count or 0

	-- Already loaded.  Tank.
	if self.images[handle] ~= nil then return self.images[handle].img end

	self.images[handle] = {}
	self.images[handle].quads = {}

	local tempImg = love.graphics.newImage(filePath)

	if tempImg ~= nil then 
		self.images[handle].img = tempImg
		self.count = self.count + 1
	end

	return self.images[handle].img

end

function ImageLibrary:loadSet(s_handle, s_file, s_img)

	local handle = s_handle

	if handle == nil then return nil end

	if s_file == nil then return nil end

	local fp,p,f = pathToFile(s_file)

	--if s_img == nil then return nil end

	-- Build the base table if necessary
	if self.images == nil then self.images = {} end
	if self.imageSrc == nil then self.imageSrc = {} end

	-- Already loaded.  Tank.
	if self.images[handle] ~= nil then return true end

	self.images[handle] = {}

	local xmlTab = Xml.deserializeFile(s_file)
	self.images[handle].cells = tableMerge((self.images[handle].cells or {}), xmlTab.cell)

	local tempImg = nil

	if s_img ~= nil then
		tempImg = self:getImageByName(s_img)

		if tempImg ~= nil then 
			self.images[handle].img = tempImg
			self.count = (self.count or 0) + 1
		else
			error("Could not load filename: "..s_img)
		end
	end

	-- Create quads
	for qk,qv in pairs(self.images[handle].cells) do
		if(type(qv) == 'table') then
			local locImg = tempImg
			if(qv.imageFile ~= nil) then
				local imgPath = pathToFile(p .. '/' .. qv.imageFile)
				locImg = self:getImageByName(imgPath)	-- Local override
			end
			
			if(locImg ~= nil) then
				local quad = qv.quad
				local gquad = love.graphics.newQuad(quad.x, quad.y, quad.w, quad.h, locImg:getWidth(), locImg:getHeight())
				
				qv.gquad = gquad
				qv.img = locImg
			end
		end
	end

	-- Alias for compatibility
	self.images[handle].quads = self.images[handle].cells

end

function ImageLibrary:getImageByName(s_img)

	local tempImg = nil

	if(self.imageSrc[s_img] ~= nil) then
		tempImg = self.imageSrc[s_img]
	else
		-- Load the image file
		tempImg = love.graphics.newImage(s_img)
		self.imageSrc[s_img] = tempImg
	end
	
	return tempImg
	
end

function ImageLibrary:getImage(set)
	return self.images[set].img
end

function ImageLibrary:getCell(set,quad)
	local s = self.images[set]
	if(s) then
		return s.quads[quad]
	end
end

function ImageLibrary:getQuad(set,quad)
	return self.images[set].quads[quad].gquad
end

function ImageLibrary:getDim(set,quad)
	return self.images[set].quads[quad].quad.w, self.images[set].quads[quad].quad.h
end

function ImageLibrary:getq(fullId)

	local a,b,set,quad = string.find(fullId, '^(.+)%.(.+)$')
	return self.images[set].img, self.images[set].quads[quad].gquad

end

function ImageLibrary:getSpb(img)

	local spbSize = units(gconf.vpWidth) * units(gconf.vpHeight) * 4

	self.spbs[img] = self.spbs[img] or love.graphics.newSpriteBatch(img, spbSize)

	return self.spbs[img]

end
